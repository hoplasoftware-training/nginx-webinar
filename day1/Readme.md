# Day 1 Workshops
**NGINX PLUS**


The environment for this lab consists of 3 Hosts.

 - Nginx Plus Master Host (CentOS) [Nginx+_Master_Host]
 - Docker Host (CentOS) - Where the necessary backends will be located. [Docker_Host]
 - Windows Host - We will use to access the nodes and perform the checks from a web browser.
 
 Labs IP Hosts resolution:
 >      - 10.1.1.5      docker.nginx-udf.internal       [Docker_Host]
 >      - 10.1.1.6      master.nginx-udf.internal       [Nginx+_Master_Host]
 >      - 10.1.1.6      nginx-app.nginx-udf.internal    [Nginx+_Master_Host]

 
In the following laboratories we will learn:

- LAB0 - Join the course.
- LAB1 - Installation of Nginx+.
- LAB2 - Basic Balancing Setup.
- LAB3 - Using the Dashboard.
- LAB4 - Use of different balancing methods.
- LAB5 - Health Checks.
- LAB6 - Active Health Checks.
- LAB7 - Dynamic HTTP Upstream Configuration via API.


## LAB0 - Join the Course.

This lab will go through initial setup and add you the agility class.  

***NOTE***

>*a prerequisite for this exercise is having an RDP client on your machine such as remote desktop connection. 
   If you do not have one, please download one, Some examples are*
   > 
   >*Remote desktop connection (macOS)
   https://apps.apple.com/us/app/microsoft-remote-desktop/*
   >
   >*Chrome browser RDP    https://remotedesktop.google.com/*

   ---

Follow these steps to complete this lab:

**Step 1 - Setting Up Lab Workstation**

1. Open your web browser
2. Navigate to https://udf.f5.com/courses
3. Login using your UDF credentials, you should have received an email from noreply@registration.udf.f5.com

![email](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/1email.png)

4. Upon first login, you will be prompted to change your password. 

![password](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/2password.png)

5. Review and accept the terms for using UDF.

![3terms](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/3terms.png)

6. Locate the class "Nginx Plus  & integrating with BIG-IP" and click launch. 

![4launch](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/4launch.png)

7. You will be given a overview the class, press join.

![5join](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/5join.png)

8. click on the 'deployment' tab in the top left, notice your lab environment is spinning up. 

![7spinning](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/7spinning.png)

**Step 2 - RDP to Windows Jumphost**

In this exercise, we will connect to the Windows Jumphost.   

1. Under the 'Systems' column, locate the 'Jumphost' block. 

  ![8jumphost](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/8jumphost.png)


2. Click 'Access' -> 'RDP' and this will download a '.rdp' file to your local machine. 

3. Once the RDP has downloaded, open the .rdp file and when prompted, select 'continue'. 

4. When prompted for the login credentials, use username: user: password: user 

  ![9rdp](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/9rdp.png)

5. You should now be in your windows Jumphost. 

   ![windows](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/windows.png)


---

## LAB1 - Nginx+ Installation (On CentOS)

We are going to perform this lab in the Master instance of Nginx+.
In this laboratory we will perform the following steps:

- Create a directory for Nginx+ certificates
- Copy of the certificates in the directory (Previously we would have downloaded the files from the Nginx client web portal)
- Installation of dependencies (ca-certificates)
- Add Nginx+ repository
- Nginx+ Installation
- Enable the nginx service start at boot. (If you want)
- Checking the installation.

**1. Create a directory for Nginx+ certificates**
```
[Nginx+_Master_Host]$ sudo mkdir /etc/ssl/nginx
```

**2. Download certificate files into /etc/ssl/nginx directory**
(Delete the files in /etc/ssl/nginx/ first, if they exist)
```
[Nginx+_Master_Host]$ sudo curl https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/files/nginx-repo.crt --output /etc/ssl/nginx/nginx-repo.crt

[Nginx+_Master_Host]$ sudo curl https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/files/nginx-repo.key --output /etc/ssl/nginx/nginx-repo.key

```

**3. Installation of dependencies (ca-certificates)**
```
[Nginx+_Master_Host]$ sudo yum install ca-certificates
```

**4. Add Nginx+ repository**

```
[Nginx+_Master_Host]$ sudo wget -P /etc/yum.repos.d https://cs.nginx.com/static/files/nginx-plus-7.4.repo
```

**5. Nginx+ Installation**

```
[Nginx+_Master_Host]$ sudo yum install nginx-plus
```

**6. Enable the nginx service start at boot**
```
[Nginx+_Master_Host]$ sudo systemctl enable nginx.service
```

**7. Checking the installation.**
```
[Nginx+_Master_Host]$ sudo systemctl status nginx

● nginx.service - NGINX Plus - high performance web server
   Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2020-06-30 09:54:47 CEST; 2 days ago
     Docs: https://www.nginx.com/resources/
 Main PID: 1398 (nginx)
    Tasks: 3 (limit: 4659)
   CGroup: /system.slice/nginx.service
           ├─1398 nginx: master process /usr/sbin/nginx -c /etc/nginx/nginx.conf
           ├─8103 nginx: worker process
           └─8104 nginx: worker process

Jun 30 09:54:38 webinar-nginxplus-master systemd[1]: Starting NGINX Plus - high performance web server...
Jun 30 09:54:47 webinar-nginxplus-master nginx[1332]: nginx: [warn] no zones to sync for server in /etc/nginx/nginx.conf:51
Jun 30 09:54:47 webinar-nginxplus-master systemd[1]: nginx.service: Can't open PID file /var/run/nginx.pid (yet?) after start: No such file or directory
Jun 30 09:54:47 webinar-nginxplus-master systemd[1]: Started NGINX Plus - high performance web server.
```

If the service is not running we will start it.
```
[Nginx+_Master_Host]$ sudo systemctl start nginx
```
We check, with a curl, that the Nginx+ service is up and running.
```
[Nginx+_Master_Host]$ curl http://localhost

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

---


## LAB2 - Basic Balancing Setup

In this lab we're going to:
- LAB2.1 - Deploy 3 docker containers to emulate 3 backends of an application on the Docker Host.
- LAB2.2 - Deploy in the Nginx+ Master host a basic configuration file to access the Dasboard.
- LAB2.3 - Deploy in the Nginx+ Master host a basic balancing configuration file with the 3 backends configured.
- LAB2.4 - Deploy in the Nginx+ Master host a balancing configuration file with one of the backends as backup.
 

### LAB2.1 - Deploy 3 docker containers to emulate 3 backends of an application.

Once connected to the Docker Host (docker.nginx-udf.internal) we will deploy the docker containers that will simulate the backends with our application.

First stop all the containers in the Docker host.
```
[Docker_Host]$ docker container stop $(docker container ls|awk '{print $1}'|grep -v CONTAINER)
```
Now we will deploy the containers that we will use as backends for our next labs.

```
[Docker_Host]$ docker run -p 7001:3000 -d --name=red-7001 --env='COLOR=red' --cpus 0.5 frjaraur/colors:latest
[Docker_Host]$ docker run -p 8001:3000 -d --name=blue-8001 --env='COLOR=blue' --cpus 0.5 frjaraur/colors:latest
[Docker_Host]$ docker run -p 9001:3000 -d --name=green-9001 --env='COLOR=green' --cpus 0.5 frjaraur/colors:latest
```
Check that the 3 containers are running.
```
[Docker_Host]$ docker container ls 

CONTAINER ID        IMAGE                    COMMAND              CREATED             STATUS              PORTS                    NAMES
2405f896123f        frjaraur/colors:latest   "node app.js 3000"   7 seconds ago       Up 6 seconds        0.0.0.0:9001->3000/tcp   green-9001
3927331a7a47        frjaraur/colors:latest   "node app.js 3000"   14 seconds ago      Up 13 seconds       0.0.0.0:8001->3000/tcp   blue-8001
94d2b2dd34c0        frjaraur/colors:latest   "node app.js 3000"   22 seconds ago      Up 21 seconds       0.0.0.0:7001->3000/tcp   red-7001
```

### LAB2.2 - Deploy in the Nginx+ Master host a basic configuration file to access the Dasboard.

Next, we will deploy an Nginx configuration file to access the Dashboard, so that we can see all the configurations and modifications that we are going to make in the successive laboratories.
To do this, first we will rename the default configuration file.
```
[Nginx+_Master_Host]$ sudo mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.orig
```
Now we deploy the configuration file that will give us access to the Dashboard.

```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/labApi.conf' <<EOF
server {
    listen 80;
    server_name master.nginx-udf.internal;

    location /api/ {
        api write=on;
    }

    location = /dashboard.html {
        root /usr/share/nginx/html;
    }

    # Redirect requests for pre-R14 dashboard
    location /status.html {
        return 301 /dashboard.html;
    }

    location /swagger-ui/ {
        root /usr/share/nginx/html/;
        index index.html;
    }
}
EOF
```
Reload the nginx configuration
```
[Nginx+_Master_Host]$ sudo nginx -t && sudo nginx -s reload
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```
To access the Dashboard, from the Windows host web browser you will access the following url: http://master.nginx-udf.internal/dashboard.html or by clicking on the "_Nginx+ Dashboard_" bookmark in the bookmarks bar.


### LAB2.3 - Deploy in the Nginx+ Master node a basic balancing configuration with the 3 backends configured.

We deployed a base load balancing configuration.

```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/colors-basic-conf.conf' <<EOF
upstream colors {
        zone colors 64k;
        server docker.nginx-udf.internal:7001;
        server docker.nginx-udf.internal:8001;
        server docker.nginx-udf.internal:9001;
}

server {
        listen 80;
        server_name nginx-app.nginx-udf.internal;
        location /colors {
        status_zone colors;
            proxy_pass http://colors;
        }
}
EOF
```
Reload the nginx configuration
```
[Nginx+_Master_Host]$ sudo nginx -t && sudo nginx -s reload
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```
Once the nginx configuration is deployed and reloaded, from the Windows node web browser we will check the correct operation of the balancing, accessing the url http://nginx-app.nginx-udf.internal/colors, or ***we can launch the next run on the Docker Host, which will make 10,000 requests on the service we have deployed.***
We can observe how these requests are processed from the Dashboard web page.

```
[Docker_Host]$ docker run --rm -v /tmp/abdata:/tmp frjaraur/nettools:small ab -k -n10000 -c50 -H 'Host: nginx-app.nginx-udf.internal' http://10.1.1.6/colors
```

As it is a default configuration, the balancing method used will be Round-Robin, that is, it will alternate the requests to each of the backends.


### LAB2.4 Deploy in the Nginx+ Master node a balancing configuration with one of the backends as backup.
With this configuration the bakend in backup state will only attend requests when the other two backens are unavailable.
```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/colors-basic-conf.conf' <<EOF
upstream colors {
        zone colors 64k;
        server docker.nginx-udf.internal:7001;
        server docker.nginx-udf.internal:8001;
        server docker.nginx-udf.internal:9001 backup;
}

server {
        listen 80;
        server_name nginx-app.nginx-udf.internal;
        location /colors {
        status_zone colors;
            proxy_pass http://colors;
        }
}
EOF
```
Reload the nginx configuration
```
[Nginx+_Master_Host]$ sudo nginx -t && sudo nginx -s reload
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```
Once the Nginx+ configuration is displayed, we will check through the web browser or through the command line (curl, http...) that of the 3 backends configured, 2 of them (red and blue) will return requests, while the one configured as backup(green), will not do so until the other 2 are no longer available.
To do this we will stop the two containers that are active, connecting to the docker host (docker.nginx-udf.internal) and executing the following command:

```
[Docker_Host]$ docker container stop red-7001 blue-8001
```

We check, through the web browser or through command line (curl, http...), that the backend that now responds is the one that was configured as backup (green).

Before moving on to the next laboratory we start the containers (red and blue) that we had previously stopped.
From the Docker node we execute the following:

```
[Docker_Host]$ docker container start red-7001 blue-8001
```
---
## LAB3 - Using the Dashboard.

In this lab we will explore the Nginx+ Dashboard,and perform some actions that are available from it on the HTTP Upstreams.

- LAB3.1 - Deployment of a basic configuration and Access to the Dashboard.
- LAB3.2 - Exploring the Dashboard.
- LAB3.3 - Modifying the status of the backend red-7001
- LAB3.4 - Deleting the green-9001 backend. 

### LAB3.1 - Deployment of a basic configuration and Access to the Dashboard.

Deploy the next basic configuration to be able to "work" with it from the Nginx Dashboard.

```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/colors-basic-conf.conf' <<EOF
upstream colors {
        zone colors 64k;
        server docker.nginx-udf.internal:7001;
        server docker.nginx-udf.internal:8001;
        server docker.nginx-udf.internal:9001;
}

server {
        listen 80;
        server_name nginx-app.nginx-udf.internal;
        location /colors {
        status_zone colors;
            proxy_pass http://colors;
        }
}
EOF
```
Reload the nginx configuration
```
[Nginx+_Master_Host]$ sudo nginx -t && sudo nginx -s reload
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```
From the web browser we access the url http://master.nginx-udf.internal/dashboard.html to access the Dashboard.

![Dashboard1](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/Nginx_Dashboard1.png)


### LAB3.2 - Exploring the Dashboard.
Once in the Dashboard we explore the different tabs and elements.
The elements that we see in the Dashboard are according to the different services that we have configured in Nginx+. In this case we simply have a single zone with an upstream with 3 backends.

![Dashboard2](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/Nginx_Dashboard2.png)

However, if we access the url http://demo.nginx.com/ we will be able to see a demo dashboard in which we will have available all the elements that can be shown.

![DemoDashboard](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/Nginx_DemoDashboard.png)

### LAB3.3 - Modifying the status of the backend red-7001

Through the Dashboard it is possible to make certain modifications on the configurations "loaded" in Nginx. In this case, through the Dashboard, we are going to change the status of the network backend red-7001 from the current Up status to Down status. For it, we are going to do the steps numbered in the following image.

![Dashboard3](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/Nginx_Dashboard3.png)

From the Windows host web browser, and accessing the url http://nginx-app.nginx-udf.internal/colors we will check the availability of the backends (red, blue and green), refreshing the page to change backends (remember that according to the configuration displayed the balancing method is Round-Robin)  
In this case the backend blue-8001 and green-9001 must be available.

### LAB3.4 - Deleting the green-9001 backend.

Following the steps of the following image we are going to unbalance the backend green-9001.

![Dashboard4](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/Nginx_Dashboard4.png)

From the Windows host web browser, and accessing the url http://nginx-app.nginx-udf.internal/colors we will check the availability of the backends (red, blue and green), refreshing the page to change backends (remember that according to the configuration displayed the balancing method is Round-Robin).  
In this case, and if we have not changed the status of the backend red-7001 to Up, only the backend blue-8001 has to be available.

---
## LAB4 - Use of different balancing methods.
In this lab we're going to use some of the balancing methods available at Nginx.

- LAB4.1 - **weighting** Method.
- LAB4.2 - **least_conn** Method
- LAB4.3 - **least-time** Method (NGINX Plus only)
- LAB4.4 - **random** Method

### LAB4.1 - **weighting** Method.

With this configuration of weights in the backends of the service, of every 12 requests, 4 will go to backend red-7001, 6 will go to backend blue-8001 and 2 to backend green-9001.  
Deploy the nginx configuration file to the Nginx+ Master Host.

```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/colors-basic-conf.conf' <<EOF
upstream colors {
        zone colors 64k;
        server docker.nginx-udf.internal:7001 weight=4;
        server docker.nginx-udf.internal:8001 weight=6;
        server docker.nginx-udf.internal:9001 weight=2;
}

server {
        listen 80;
        server_name nginx-app.nginx-udf.internal;
        location /colors {
        status_zone colors;
            proxy_pass http://colors;
        }
}
EOF
```
Reload the nginx configuration.
```
[Nginx+_Master_Host]$ sudo nginx -t && sudo nginx -s reload
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

We can make the check from the windows node web browser and making, for example, 100 requests on the url http://nginx-app.nginx-udf.internal/colors or by running the following on the Docker Host.

```
[Docker_Host]$ docker run --rm -v /tmp/abdata:/tmp frjaraur/nettools:small ab -k -n100 -c3 -H 'Host: nginx-app.nginx-udf.internal' http://10.1.1.6/colors
```

### LAB4.2 - **least_conn** Method

Using this balancing method, requests will be routed based on the least number of active connections in the backends.  
Deploy the nginx configuration file to the Nginx+ Master Host.

```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/colors-basic-conf.conf' <<EOF
upstream colors {
        zone colors 64k;
        least_conn;
        server docker.nginx-udf.internal:7001;
        server docker.nginx-udf.internal:8001;
        server docker.nginx-udf.internal:9001;
}

upstream stress {
        zone colors 64k;
        server docker.nginx-udf.internal:9001;
}

server {
        listen 80;
        server_name nginx-app.nginx-udf.internal;
        location /colors {
        status_zone colors;
            proxy_pass http://colors;
        }
        location /stress {
        status_zone colors;
            proxy_pass http://stress;
        }
}
EOF
```
Reload the nginx configuration.
```
[Nginx+_Master_Host]$ sudo nginx -t && sudo nginx -s reload
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```
In this case we will make a greater number of connections on the backend green-9001. 

To do this from the Docker Host we'll run the following:

```
[Docker_Host]$ docker run --rm -v /tmp/abdata:/tmp frjaraur/nettools:small ab -k -n6000 -c100 -H 'Host: nginx-app.nginx-udf.internal' http://10.1.1.6/stress & docker run --rm -v /tmp/abdata:/tmp frjaraur/nettools:small ab -k -n6000 -c100 -H 'Host: nginx-app.nginx-udf.internal' http://10.1.1.6/colors
```
From the web browser of the Windows Host we will be able to observe the behavior of the balancing in the Dashboard's web.

### LAB4.3 - **least-time** Method (NGINX Plus only)

For each request, NGINX Plus selects the server with the lowest average latency and the lowest number of active connections.

The parameters that can be used with the least_time directive are as follows:
- _header_ – Time to receive the first byte from the server
- _last_byte_ – Time to receive the full response from the server
- _last_byte inflight_ – Time to receive the full response from the server, taking into account incomplete requests.

Deploy the nginx configuration file to the Nginx+ Master Host.

```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/colors-basic-conf.conf' <<EOF
upstream colors {
        zone colors 64k;
        least_time last_byte;
        server docker.nginx-udf.internal:7001;
        server docker.nginx-udf.internal:8001;
        server docker.nginx-udf.internal:9001;
}


server {
        listen 80;
        server_name nginx-app.nginx-udf.internal;
        location /colors {
        status_zone colors;
            proxy_pass http://colors;
        }
}
EOF
```
Reload the nginx configuration.
```
[Nginx+_Master_Host]$ sudo nginx -t && sudo nginx -s reload
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```
To get a correct view of this balancing method we are going to generate stress in the backend blue-8001 so that when responding to requests, its response time is longer than the rest of the backends.

To do this we'll run the following from the Docker Host:

```
[Docker_Host]$ docker run --rm -v /tmp/abdata:/tmp frjaraur/nettools:small ab -k -n6000 -c100 -H 'Host: nginx-app.nginx-udf.internal' http://10.1.1.6/colors & docker run --rm -v /tmp/abdata:/tmp frjaraur/nettools:small ab -k -n6000 -c100 -H 'Host: nginx-app.nginx-udf.internal' http://10.1.1.5:8001/
```

From the web browser of the windows host we will be able to observe the behavior of the balancing in the Dashboard's web.


### LAB4.4 - **random** Method

With this method of balancing, each request will be passed to a randomly selected server. If the _two_ parameter is specified, first, NGINX randomly selects two servers taking into account server weights, and then chooses one of these servers using the specified method:
 - least_conn
 - least_time=header
 - least_time=last_byte

In this lab we will use the Random method with parameter _two_ and as a second balancing system we will use _least_time=last_byte_.

Deploy the next nginx configuration file to the Nginx+ Master host.

```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/colors-basic-conf.conf' <<EOF
upstream colors {
        zone colors 64k;
        random two least_time=last_byte;
        server docker.nginx-udf.internal:7001 weight=4;
        server docker.nginx-udf.internal:8001 weight=6;
        server docker.nginx-udf.internal:9001 weight=2;
}


server {
        listen 80;
        server_name nginx-app.nginx-udf.internal;
        location /colors {
        status_zone colors;
            proxy_pass http://colors;
        }
}
EOF
```
Reload the nginx configuration.
```
[Nginx+_Master_Host]$ sudo nginx -t && sudo nginx -s reload
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```
As in the previous lab to get a correct view of this balancing method we are going to generate stress in the backend blue-8001 so that when responding to requests, its response time is longer than the rest of the backends.

To do this we'll run the following from the Docker host:

```
[Docker_Host]$ docker run --rm -v /tmp/abdata:/tmp frjaraur/nettools:small ab -k -n6000 -c100 -H 'Host: nginx-app.nginx-udf.internal' http://10.1.1.6/colors & docker run --rm -v /tmp/abdata:/tmp frjaraur/nettools:small ab -k -n6000 -c100 -H 'Host: nginx-app.nginx-udf.internal' http://10.1.1.5:8001/
```
From the web browser of the windows host we will be able to observe the behavior of the balancing in the Dashboard's web.

---

## LAB5 - Passive Health Checks.

In this lab we will perform an example of a passive health check.
We will configure one of the backends so that Nginx marks it as unavailable for 30 seconds after 3 consecutive failed checks, and marks it again as available after the first successful check after 30 seconds from the last failed check.

Deploy the next nginx configuration file to the Nginx+ Master host.

```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/colors-basic-conf.conf' <<EOF
upstream colors {
        zone colors 64k;
        server docker.nginx-udf.internal:7001;
        server docker.nginx-udf.internal:8001;
        server docker.nginx-udf.internal:9001 max_fails=3 fail_timeout=30s;
}

server {
        listen 80;
        server_name nginx-app.nginx-udf.internal;
        location /colors {
        status_zone colors;
            proxy_pass http://colors;
        }
}
EOF
```
Reload the nginx configuration.
```
[Nginx+_Master_Host]$ sudo nginx -t && sudo nginx -s reload
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```
From the Windows host web browser, and accessing the url http://nginx-app.nginx-udf.internal/colors we will check the availability of the backends (red, blue and green), refreshing the page to change backends (remember that according to the configuration displayed the balancing method is Round-Robin)

Now let's stop one of the backends (green-9001) from providing service, for them we'll run the following on the Docker host
```
[Docker_Host]$ docker container stop green-9001
```

In the web browser of the Windows host we will open another window to connect to the Dashboard (http://master.nginx-udf.internal/dashboard.html). Having the 2 windows at sight, we refresh the page http://nginx-app.nginx-udf.internal/colors (3 times per color) until we see that the backend green no longer answers us, and in the Dashboard window, in the Upstreams tab we check that the backend green-9001 has marked it as unavailable.

Once the unavailability of the backend has been verified (green-9001) we lift it again. To do this, we run on Docker's host:
```
[Docker_Host]$ docker container start green-9001
```

Once the backend is up, we go back to the Windows host and refresh the web of colors again. e will verify that, once the backend has been lifted, it will not be available again until the time indicated by the _fail_timeout_ directive has passed after the last failed check, in this case it will be passed 30 seconds since the last failed check.

---

## LAB6 - Active Health Checks.

In this lab we will see several examples of active health checks, which are exclusive to the Nginx+ version.

### LAB6.1 - Health Check Directive

We'll start with something basic like setting the health_check directive, by default, on our backends.

Deploy the next nginx configuration file to the Nginx+ Master host.

```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/colors-basic-conf.conf' <<EOF
upstream colors {
        zone colors 64k;
        server docker.nginx-udf.internal:7001;
        server docker.nginx-udf.internal:8001;
        server docker.nginx-udf.internal:9001;
}

server {
        listen 80;
        server_name nginx-app.nginx-udf.internal;
        location /colors {
        status_zone colors;
            proxy_pass http://colors;
            health_check;
        }
}
EOF
```
Reload the nginx configuration.
```
[Nginx+_Master_Host]$ sudo nginx -t && sudo nginx -s reload
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

In the web browser of the Windows host we go back to the Dashboard (http://master.nginx-udf.internal/dashboard.html), and in the Upstreasm tab, in the Health monitors column we can see the checks that Nginx+ is actively performing on the backends.

To check how Nginx+ actively monitors the availability of the backends, let's stop and pick up one of them and check on the Dashboard website how it automatically marks it as available or unavailable. 
To do this we'll first stop one of the backends from the Docker host.

```
[Docker_Host]$ docker container stop green-9001
```

Check, from the Dashboard, how Nginx+ automatically marks it as unavailable.

We restart the backend green-9001 from the Docker host and check again, from the Dashboard, that Nginx+ marks it as available again as soon as the check has access to it.

```
[Docker_Host]$ docker container start green-9001
```
### LAB6.2 - Modify default Health Check config.

In the following example we will modify the default configuration of the health_check directive.
Let's set it up:
- _interval_ - Check interval
- _fails_  - Number of failed checks to mark the backend as unavailable
- _passes_ - Number of correct checks to mark the backend as available

Deploy the next nginx configuration file to the Nginx+ Master host.

```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/colors-basic-conf.conf' <<EOF
upstream colors {
        zone colors 64k;
        server docker.nginx-udf.internal:7001;
        server docker.nginx-udf.internal:8001;
        server docker.nginx-udf.internal:9001;
}

server {
        listen 80;
        server_name nginx-app.nginx-udf.internal;
        location /colors {
        status_zone colors;
            proxy_pass http://colors;
            health_check interval=8 fails=5 passes=2;
        }
}
EOF
```
Reload the nginx configuration.
```
[Nginx+_Master_Host]$ sudo nginx -t && sudo nginx -s reload
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```
In the web browser of the Windows host we go back to the Dashboard (http://master.nginx-udf.internal/dashboard.html), and in the Upstreasm tab, in the Health monitors column we can see the checks that Nginx+ is actively performing on the backends.

To check how Nginx+ actively monitors the availability of the backends, let's stop and pick up one of them and check on the Dashboard website how it automatically marks it as available or unavailable. 
To do this we'll first stop one of the backends (red-7001) from the Docker Host.

```
[Docker_Host]$ docker container stop red-7001
```

It checks, from the Dashboard, how Nginx+ marks the backend red-7001 as unavailable after 5 consecutive failed checks, each performed within 8 seconds, which are the parameters set in the health_check directive configuration.

We restart the backend red-7001 (from the Docker Host) we have previously stopped.
```
[Docker_Host]$ docker container start red-7001
```
And now, in the web browser of the Windows host we go back to the Dashboard (http://master.nginx-udf.internal/dashboard.html), and in the Upstreasm tab, in the Health monitors column we can see the checks that Nginx+ is actively performing on the backends.



### LAB6.3 - Custom Conditions

Defining Custom Conditions

In the next lab we are going to create a custom condition to use it as a parameter of the health_check directive when monitoring the availability of the backends.

In this case we'll check the content of the body block and the backend that doesn't contain the network text in that block will be considered "good", and also the status code returned will be between 200 and 399.

Deploy the next nginx configuration file to the Nginx+ Master host.

```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/labMatch.conf' <<EOF

match colors_ok {
    status 200-399;
    body ~ " red";
}
EOF
```

```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/colors-basic-conf.conf' <<EOF
upstream colors {
        zone colors 64k;
        server docker.nginx-udf.internal:7001;
        server docker.nginx-udf.internal:8001;
        server docker.nginx-udf.internal:9001;
}

server {
        listen 80;
        server_name nginx-app.nginx-udf.internal;
        location /colors {
        status_zone colors;
            proxy_pass http://colors;
            health_check match=colors_ok;
        }
}
EOF
```
Reload the nginx configuration.
```
[Nginx+_Master_Host]$ sudo nginx -t && sudo nginx -s reload
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

We will check from the web browser of the host windows, in the Dashboard page, that the backend with the red text in the body, Nginx+, through the active health check, will mark it immediately as unavailable.

We can change the content of the labMatch.conf configuration file to generate unavailability in the other bakcends.

---

## LAB7 - Dynamic HTTP Upstream Configuration via API.

In the next lab we will make modifications in the upstreams through the API.
We'll see how to add and remove backend servers from the upstream, and how to change their status, Up, Down and Drain.

- LAB7.1 - Deployment of basic configuration of balancing with 2 backends.
- LAB7.2 - Add the backend blue-9001 in down status.
- LAB7.3 - Change the status of the blue-9001 backend from down to up.
- LAB7.4 - Removal of the backend red-7001 from the upstream configuration.

### LAB7.1 - Deployment of basic configuration of balancing with 2 backends.
To do this we will deploy a round-robin configuration with 2 servers in the upstream, red-7001 and green-9001.

We deploy a basic nginx configuration file.

```
[Nginx+_Master_Host]$ sudo bash -c 'cat > /etc/nginx/conf.d/colors-basic-conf.conf' <<EOF
upstream colors {
        zone colors 64k;
        server docker.nginx-udf.internal:7001;
        server docker.nginx-udf.internal:9001;
}

server {
        listen 80;
        server_name nginx-app.nginx-udf.internal;
        location /colors {
        status_zone colors;
            proxy_pass http://colors;
        }
}
EOF
```
Reload the nginx configuration
```
[Nginx+_Master_Host]$ sudo nginx -t && sudo nginx -s reload
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

From the Web browser we check in the Dashboard the deployment that we have done.

### LAB7.2 - Add the backend blue-8001 in down status.

Now we register a new server, blue-8001, in the upstream group by means of a call through the API. This new server will be added with the status down.
To do so, we execute the following command from the Nginx+ Master host.
```
[Nginx+_Master_Host]$ curl -X POST "http://master.nginx-udf.internal/api/6/http/upstreams/colors/servers/" \
-H "accept: application/json" \
-H "Content-Type: application/json" \
-d "{ \"server\": \"10.1.1.5:8001\", \
\"weight\": 1, \
\"max_conns\": 0, \
\"max_fails\": 1, \
\"fail_timeout\": \"10s\", \
\"slow_start\": \"0s\", \
\"route\": \"\", \
\"backup\": false, \
\"down\": true}"
```

From the Dashboard web page we check that a new server has appeared in the upstream, and its state is down.

If from the web browser we access the url of the application http://nginx-app.nginx-udf.internal/colors we can check that the new backend/server that we have added, blue-8001, does not answer requests.

### LAB7.3 - Change the status of the blue-8001 backend from down to up.

Now we are going to change, also through the API, the server status :8001 from the current down to up.

```
[Nginx+_Master_Host]$ curl -X PATCH -d '{ "down": false }' \
-s 'http://master.nginx-udf.internal/api/6/http/upstreams/colors/servers/2'
```

Once again if from the web browser we access the url of the application http://nginx-app.nginx-udf.internal/colors we can check that the backend/server blue-8001 is already responding to requests.


### LAB7.4 - Removal of the backend red-7001 from the upstream configuration.

Finally, and by using the API, we are going to remove the backend red-7001 from the upstream configuration.
To do this, first we will find out the id of the server to be deleted, executing the following request.

```
[Nginx+_Master_Host]$ curl -X GET \
http://master.nginx-udf.internal/api/6/http/upstreams/colors/servers|jq
```

Once we have the id of the server we want to remove, in this case id=0, we'll execute the next request to remove it from the configuration.

```
[Nginx+_Master_Host]$ curl -X DELETE \
-s 'http://master.nginx-udf.internal/api/6/http/upstreams/colors/servers/0'
```

From the Dashboard website we can see that the backend/server red-7001 has disappeared and, from the web browser, if we access the url of the application http://nginx-app.nginx-udf.internal/colors we can check that the backend/server that we have removed, red-7001, does not answer requests.




