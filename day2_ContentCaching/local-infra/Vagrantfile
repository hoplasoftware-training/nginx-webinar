# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'

class String
    def black;          "\e[30m#{self}\e[0m" end
    def red;            "\e[31m#{self}\e[0m" end
    def cyan;           "\e[36m#{self}\e[0m" end
end


engine_version=''
engine_mode='default'
proxy = ''

ENV['VAGRANT_DEFAULT_PROVIDER'] = 'virtualbox'

config = YAML.load_file(File.join(File.dirname(__FILE__), 'instances.local.yml'))

base_box=config['environment']['base_box']
base_box_version=config['environment']['base_box_version']

engine_version=config['environment']['engine_version']

boxes = config['boxes']

boxes_hostsfile_entries=""

domain=config['environment']['domain']

osflavour=config['environment']['base_box_osflavour']

docker_url=config['environment']['docker_url']

########

deb_ansible_enablement = <<SCRIPT
  DEBIAN_FRONTEND=noninteractive apt-get install -qq python
  useradd -m -s /bin/bash provision
  mkdir -p ~provision/.ssh
  cp /vagrant/keys/*.pub ~provision/.ssh/authorized_keys
  chown -R provision:provision ~provision/.ssh
  chmod -R 700 ~provision/.ssh
  echo "provision ALL=(ALL) NOPASSWD:ALL" >/etc/sudoers.d/provision
  echo "Defaults:provision !requiretty" >>/etc/sudoers.d/provision
SCRIPT


rh_ansible_enablement = <<SCRIPT
  yum install -q -y python
  useradd -m -s /bin/bash provision
  mkdir -p ~provision/.ssh
  cp /vagrant/keys/*.pub ~provision/.ssh/authorized_keys
  chown -R provision:provision ~provision/.ssh
  chmod -R 700 ~provision/.ssh
  echo "provision ALL=(ALL) NOPASSWD:ALL" >/etc/sudoers.d/provision
  echo "Defaults:provision !requiretty" >>/etc/sudoers.d/provision
SCRIPT


deb_common_software = <<SCRIPT
  systemctl disable apt-daily.service
  systemctl disable apt-daily.timer
  DEBIAN_FRONTEND=noninteractive apt-get update -qq
  DEBIAN_FRONTEND=noninteractive apt-get install -qq chrony curl
  timedatectl set-timezone Europe/Madrid
SCRIPT


rh_common_software = <<SCRIPT
  yum install -q -y chrony curl
  timedatectl set-timezone Europe/Madrid
SCRIPT

boxes.each do |box|
  boxes_hostsfile_entries=boxes_hostsfile_entries+box['mgmt_ip'] + ' ' +  box['name'] + ' ' +  box['name'] + "." + domain + '\n'
end

#puts boxes_hostsfile_entries

$update_hosts = <<SCRIPT
    echo "127.0.0.1 localhost" >/etc/hosts
    echo -e "#{boxes_hostsfile_entries}" |tee -a /etc/hosts
SCRIPT

puts '--------------------------------------------------------------------------------------------'

puts ' NGINX Vagrant Environment on ' + osflavour

puts ' Docker Engine Version: '+engine_version

puts '--------------------------------------------------------------------------------------------'

$install_docker_engine = <<SCRIPT
  curl -sSk $1 | sh
  usermod -aG docker vagrant 2>/dev/null
  systemctl start docker
  systemctl enable docker
SCRIPT


Vagrant.configure(2) do |config|
  if Vagrant.has_plugin?("vagrant-proxyconf")
    if proxy != ''
        puts " Using proxy"
        config.proxy.http = proxy
        config.proxy.https = proxy
        config.proxy.no_proxy = "localhost,127.0.0.1"
    end
  end
  config.vm.box = base_box
  config.vm.box_version = base_box_version
  config.vm.synced_folder "scripts/", "/scripts",create:true
  case engine_version
    when "experimental"
        engine_download_url="https://experimental.docker.com"
    when "current", "latest", "stable"
        engine_download_url="https://get.docker.com"
    when "test", "testing", "rc"
        engine_download_url="https://test.docker.com"
    else
        STDERR.puts "Unknown Docker Engine version, please use 'experimental', 'test' or 'stable'".red
         exit
    end

  boxes.each do |node|
    config.vm.define node['name'] do |config|
      config.vm.hostname = node['name']
      config.vm.provider "virtualbox" do |v|
        v.name = node['name']
        v.customize ["modifyvm", :id, "--memory", node['mem']]
        v.customize ["modifyvm", :id, "--cpus", node['cpu']]
	      v.customize ["modifyvm", :id, "--macaddress1", "auto"]
        v.customize ["modifyvm", :id, "--nictype1", "Am79C973"]
        v.customize ["modifyvm", :id, "--nictype2", "Am79C973"]
        v.customize ["modifyvm", :id, "--nictype3", "Am79C973"]
        v.customize ["modifyvm", :id, "--nictype4", "Am79C973"]

        v.customize ["modifyvm", :id, "--nicpromisc1", "allow-all"]
        v.customize ["modifyvm", :id, "--nicpromisc2", "allow-all"]
        v.customize ["modifyvm", :id, "--nicpromisc3", "allow-all"]
        v.customize ["modifyvm", :id, "--nicpromisc4", "allow-all"]
        v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      end

      config.vm.network "private_network",
        ip: node['mgmt_ip'],
        virtualbox__intnet: true


      config.vm.network "private_network",
        ip: node['hostonly_ip']

      # config.vm.network "public_network",
      # bridge: ["enp4s0","wlp3s0","enp3s0f1","wlp2s0"],
      # auto_config: true

      config.vm.provision :shell, :inline => $update_hosts
      case osflavour
        when "redhat"
          config.vm.provision :shell, :inline => rh_ansible_enablement
          config.vm.provision :shell, :inline => rh_common_software
        when "debian"
          config.vm.provision :shell, :inline => deb_ansible_enablement
          config.vm.provision :shell, :inline => deb_common_software
        else
          STDERR.puts "Unknown OS Flavour, please check instances file " + 'instances.local.yml' + ".".red
           exit
      end


      if node['role'] == "docker"
        config.vm.provision "shell" do |s|
          s.name       = "Install Docker Engine from " + docker_url
          s.inline     = $install_docker_engine
          s.args       = docker_url
        end
      end

    end
  end

end
