#!/bin/bash

sudo systemctl restart docker

sudo docker rm -fv picsum > /dev/null 2>&1


echo -e "\n## Deploying PICSUM application on port 8005"

sudo docker run -d --name picsum \
-e MAX_NUMBER_OF_PICTURES=20 \
-e MAX_PICTURE_SOURCE_URL=https://picsum.photos/1024 \
-p 8005:80 \
--restart=unless-stopped \
bosix/test-picture-provider
