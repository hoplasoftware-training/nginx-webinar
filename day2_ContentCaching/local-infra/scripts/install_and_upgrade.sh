#!/bin/bash

OSFLAVOUR="redhat"

[ -f /etc/lsb-release ] && OSFLAVOUR="debian"

sudo mkdir -p /etc/ssl/nginx

if [ ! -f ./nginx-repo/nginx-repo.crt ]
then
	sudo curl -s -o /etc/ssl/nginx/nginx-repo.crt https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/files/nginx-repo.crt
else
	sudo cp ./nginx-repo/nginx-repo.crt /etc/ssl/nginx/nginx-repo.crt
fi

if [ ! -f ./nginx-repo/nginx-repo.key ]
then
	sudo curl -s -o /etc/ssl/nginx/nginx-repo.key https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/files/nginx-repo.key
else
        sudo cp ./nginx-repo/nginx-repo.key /etc/ssl/nginx/nginx-repo.key
fi





case ${OSFLAVOUR} in
    debian)
        sudo curl -o /tmp/nginx_signing.key https://nginx.org/keys/nginx_signing.key
        sudo apt-key add /tmp/nginx_signing.keys
        sudo apt-get install -qq apt-transport-https lsb-release ca-certificates
        printf "deb https://plus-pkgs.nginx.com/ubuntu `lsb_release -cs` nginx-plus\n" | sudo tee /etc/apt/sources.list.d/nginx-plus.list
        sudo wget -q -O /etc/apt/apt.conf.d/90nginx https://cs.nginx.com/static/files/90nginx
        sudo apt-get -qq update
        sudo apt-get install -qq nginx-plus
    ;;

    redhat)
        sudo yum -q -y install ca-certificates
        sudo wget -P /etc/yum.repos.d https://cs.nginx.com/static/files/nginx-plus-7.4.repo
        sudo yum -q -y install nginx-plus
        sudo systemctl enable nginx.service
        sudo systemctl start nginx.service

    ;;

    *)
    echo "This script will only work on Debian-like and RedHat-like Linus flavours." && exit 1
    ;;
esac
