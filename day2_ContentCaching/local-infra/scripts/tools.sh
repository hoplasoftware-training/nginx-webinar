#!/bin/bash

OSFLAVOUR="redhat"

[ -f /etc/lsb-release ] && OSFLAVOUR="debian"

case ${OSFLAVOUR} in
    debian)
        sudo apt-get -qq update
        sudo apt-get install -qq httpie
    ;;

    redhat)
        sudo yum -q -y install httpie
    ;;

    *)
    echo "This script will only work on Debian-like and RedHat-like Linus flavours." && exit 1
    ;;
esac