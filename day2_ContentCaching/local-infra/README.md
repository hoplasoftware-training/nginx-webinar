# Local Labs environment

## This repository will create N working nodes ready to provision Nginx Plus.
----
## Requirements (follow each product guide):
 - Install Vagrant - https://www.vagrantup.com/
 - Install Virtualbox - https://www.virtualbox.org/

----

## Basic Usage:

1. Download or clone this respository

2. Execute **vagrant up** to create a new environment.

3. This will create nodes defined in **instances.yml** with latest Docker Engine installed (it is configurable on instances.yml).

 Default node names:
  * master (commented by default) - 2vCPU and 2GB Memory.
  * plus2 - 2vCPU and 2GB Memory.
  * plus3 - 2vCPU and 2GB Memory.
  * docker - 2vCPU and 2GB Memory.

>__NOTE: It is possible to change the number of virtual node instances and their resources changing their settings in _instances.yml_ file.__


1. Connect to nodes using vagrant as usual (**vagrant ssh node1**).
>NOTE:
>
>It could be useful to define simple alias:
> alias vssh='vagrant ssh'
>

2. When you have finnished all your labs, simple execute **vagrant destroy -f**. This will delete all virtual nodes.

