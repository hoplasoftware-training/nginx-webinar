# Day 2 Lab (Short-Session)

>NOTE: Additional information and easy copy&pasting will be available in [http://dontpad.com/HoplaWorkshops](http://dontpad.com/HoplaWorkshops).



  ![lab_description.png](./images/lab_description.png)

> NOTE: These lab can be executed offline using Vagrant and Virtualbox virtual machines. A full deployment is provided using the same IP addresses mentioned in the cloud environment. You can deploy this environment in your laptop or PC if you meet following requirements:
>
>- Working Internet connection.
>- A minimum of 8GB of RAM, 4vCPU and 30GB of free disk space.
>- Vagrant 2.2.7 or newer.
>- Virtualbox 6.0 or newer.
>
>Vagrant virtual environment should be deployed from __local-infra__ directory using ___vagrant up___ command. This will run 3 nodes by default:
>- docker - 10.1.1.5 (internal only)/192.168.56.5 (host-to-guest)
>- plus2 - 10.1.1.7 (internal only)/192.168.56.7 (host-to-guest)
>- plus3 - 10.1.1.8 (internal only)/192.168.56.8 (host-to-guest)
>
>
>__NOTE: You can find additional information in [Local Infra Readme](./local-infra/README.md)__
---

## Prepare Environment for the labs

These are common steps that may be automated. Check if these configurations are already done in your environment to avoid future problems during the session.

- First, start all required nodes by just executing __vagrant up__ inside ___nginx-webinar/day2_short-session/local-infra___ directory

```
[local-infra]$ vagrant up
--------------------------------------------------------------------------------------------
 NGINX Vagrant Environment on redhat
 Docker Engine Version: current
--------------------------------------------------------------------------------------------
Bringing machine 'docker' up with 'virtualbox' provider...
Bringing machine 'plus2' up with 'virtualbox' provider...
Bringing machine 'plus3' up with 'virtualbox' provider.
```

- Connect to each node using __vagrant ssh <NODE>__ (open at least one terminal for each node).

- Disable Firewalld for these labs on all nodes (__plus2__, __plus3__ and __docker__).
```
[plus2/plus3/docker]$ sudo systemctl stop firewalld

[plus2/plus3/docker]$ sudo systemctl disable firewalld
```

>NOTE: Disabling the Firewall and SELinux can be deployed using __/vagrant/scripts/prepare_node.sh__


- Disable SELinux for these labs on all nodes (__plus2__, __plus3__ and __docker__).
```
[plus2/plus3/docker]$ sudo getenforce
Enforcing

[plus2/plus3/docker]$ sudo setenforce 0

[plus2/plus3/docker]$ sudo getenforce
Permissive


[plus2/plus3/docker] sudo sed -i "s/SELINUX=.*/SELINUX=disabled/" /etc/sysconfig/selinux

```

If you are using Vagrant provided environment, we need to install Ngnix and Docker Engine. We provided you few scripts to automate this process.

- Install current Nginx release on __plus2__ and __plus3__ nodes. For this to work, you must download an NGINX Plus trial license in order to install the required Nginx Plus release. Once registered, download you __"nginx-repo.crt"__ and __"nginx-repo.key"__ to ___local-infra/scripts/nginx-repo/___ directory. These files will be used during installation process.
```
[plus2/plus3]$ cd /vagrant/scripts/
[plus2/plus3]$ ./install_and_upgrade.sh
```

- Deploy applications' backends on __docker__ node:
```
[docker]$ cd /vagrant/scripts/
[docker]$ ./deploy_app_backends.sh
```

```
[docker]$ cat <<EOF >deploy_app_backends.sh
#!/bin/bash

sudo systemctl restart docker

sudo docker rm -fv picsum > /dev/null 2>&1

echo -e "\n## Deploying PICSUM application on port 8005"

sudo docker run -d --name picsum \
-e MAX_NUMBER_OF_PICTURES=20 \
-e MAX_PICTURE_SOURCE_URL=https://picsum.photos/1024 \
-p 8005:80 \
--restart=unless-stopped \
bosix/test-picture-provider

EOF
```

- Deploy applications' backends on __docker__ node:
```
[docker]$ cd /vagrant/scripts/
[docker]$ ./deploy_app_backends.sh
```

- Install some interesting tools (__httpie__) on __docker__ node:
```
[docker]$ cd /vagrant/scripts/
[docker]$ ./tools.sh
```

---

# Caching Lab

## Configure Nginx Plus Dashboard

1 - Enabling Dashboard on __plus2__ and __lab3__ nodes.
```
[plus2/plus3]$ cat <<EOF |sudo tee /etc/nginx/conf.d/dashboard.conf

server {
    listen 8090;
    location /api {
      limit_except GET {
        auth_basic "NGINX Plus API";
        auth_basic_user_file /etc/nginx/.API_passwd;
      }
      api   write=on;
      allow 192.168.56.0/24;
      allow 10.1.1.0/24;
      deny  all;
    }
    location = /dashboard.html {
        root   /usr/share/nginx/html;
    }
    location /swagger-ui {
        root   /usr/share/nginx/html;
    }
}
EOF
```

2 - We generate admin credentials to perform Dashboard modifications.
```
[plus2/plus3]$ echo -n 'admin:'|sudo tee -a /etc/nginx/.API_passwd
[plus2/plus3]$ echo -n 'secret' | openssl passwd -apr1 -stdin|sudo tee -a /etc/nginx/.API_passwd
```


3 - Reload NGINX
```
[plus2/plus3]$ sudo nginx -t

[plus2/plus3]$ sudo nginx -s reload
```

4 - Dashboard for nodes __plus2__ and __plus3__ will be available on http://192.168.56.7:8090/dashboard.html for internal network and http://192.168.56.8:8090/dashboard.html, respectively.


## Caching content with Nginx.

In this lab we will learn how to provide content caching using Nginx.


1 - Configure Picsum application backend on __plus2__ and __lab3__ nodes. This application will show us a different picture on each request.
```
[plus2/plus3]$ cat <<EOF |sudo tee /etc/nginx/conf.d/picsum.conf

upstream picsum {
    server 10.1.1.5:8005;
}

server {
    listen 8080 default_server;
    error_log /var/log/nginx/picsum.error.log info;
    access_log /var/log/nginx/picsum.access.log combined;
    status_zone picsum;

    location / {
        proxy_pass http://picsum;
    }
}
EOF
```

Reload NGINX
```
[plus2/plus3]$ sudo nginx -t

[plus2/plus3]$ sudo nginx -s reload
```

We can use our browser to obtain images on http://192.168.56.7:8080/ and http://192.168.56.8:8080/ using your web browser if you are running provided Vagrant environment.


If you refresh your browser, you will get a new image.

![](./images/lab2_step1_1.jpg)

![](./images/lab2_step1_2.jpg)

![](./images/lab2_step1_3.jpg)

Let's configure caching on __plus2__ node.

2 - Configure caching for Picsum application backend on __plus2__ node.

>__IMPORTANT NOTE: We are using \\\$ to avoid variable substitution using tee to create Nginx configuration files. Override "\\" characters before "$" if you edit these files by yourself without using _cat_ or _echo_.__
```
[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/picsum.conf

upstream picsum {
    server 10.1.1.5:8005;
}

proxy_cache_path /tmp/cache keys_zone=picsumCache:10m inactive=60m;

proxy_cache_min_uses 5;

proxy_cache_valid any 10s;

proxy_cache_key "\$request_uri";

server {
    listen 8080 default_server;
    error_log /var/log/nginx/picsum.error.log info;
    access_log /var/log/nginx/picsum.access.log combined;
    status_zone picsum;

    location / {
        proxy_pass http://picsum;
        proxy_cache picsumCache;
        add_header X-Proxy-Cache \$upstream_cache_status;
    }
}
EOF
```

Reload NGINX
```
[plus2]$ sudo nginx -t

[plus2]$ sudo nginx -s reload
```

3 - Let's verify again this new behavior using __plus2__ published application on port 8080. We can use our browser to obtain images on http://192.168.56.7:8080.

![](./images/lab2_step2_1.jpg)

We have to browse more than 5 times our picsum backend, because we added __proxy_cache_min_uses__ directive with a value of 5.

After the 4th try, if we refresh in less than 10 seconds, we will obtain the same image because it is cached. Cache will be invalid in 10 seconds (__proxy_cache_valid any 10s__).
![](./images/lab2_step2_2.jpg)

If we wait more than 10 seconds, and refresh again, we will get a different image.

![](./images/lab2_step2_5.jpg)

>NOTE: First four requests will get a __MISS__ cache status because caching will not get involved until __proxy_cache_min_uses__ value is reached.

We have to notice that __plus3__ node does not work like __plus2__. We have not configured image caching on that node hence refreshing will show us different pictures. We can verify this with our browser to obtain images on http://10.1.1.8:8080.

>NOTE: Verify accesses to http://192.168.56.8:8080/ using your web browser if you are running provided Vagrant environment.

![](./images/lab2_step2_3.jpg)

Now we refresh and get a different image.7

![](./images/lab2_step2_4.jpg)

5 - We can now take a look at __/tmp/cache__ directory on __plus2__ node.
```
[plus2$ sudo ls -lRt /tmp/cache/
/tmp/cache/:
total 220
-rw-------. 1 nginx nginx 223691 jul 16 11:53 d41d8cd98f00b204e9800998ecf8427e
```

We only have one file because we requested just one url, "/", and from only one client. We configured __proxy_cache_key__ variable to use __"$request_uri"__. Therefore, cache will be evaluated on each requested URI.


As we change backends URL, images will also change. This is due to that caching key is configured to match only requested URI. We can verify this behavior with our browser using http://10.1.1.7:8080/test.

>NOTE: Verify accesses to http://192.168.56.7:8080/test using your web browser if you are running provided Vagrant environment.

![](./images/lab2_step2_7.jpg)

If we test again in less than 10 seconds using a new URI, we get a new image.

![](./images/lab2_step2_8.jpg)

We can verify this behavior with our browser using http://192.168.56.7:8080/test/1.

But if we refresh this new URL, we get the cached image.

![](./images/lab2_step2_9.jpg)


6 - We can use ___http___ from "docker" node instead of web browser to verify headers with cache information. Let's try a couple of times. We should be able to use any node in this lab to execute these tests.

In this case we don't care about images, we will take alook at __X-Proxy-Cache__ header. We will execute ____http http://192.168.56.7:8080/____ four times.
Try to execute them fast, don't expend more than 10 seconds between executions to avoid cache invalidation.
```
[docker]$ http http://192.168.56.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 10:57:37 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: EXPIRED



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

Again
```
[docker]$ http http://192.168.56.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 10:57:39 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

And again
```
[docker]$ http http://192.168.56.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 10:57:40 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

And finally
```
[docker]$ http http://192.168.56.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 10:57:42 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

Notice that first request get a "X-Proxy-Cache: EXPIRED" line. This is due to this is the first time we make this request. Nginx will load cached data and use it on subsequent requests. That's why we get "X-Proxy-Cache: HIT".

If we start again this sequence of executions, cache will be invalid again because this request is executed after 10 seconds.

```
[docker]$ http http://192.168.56.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 11:04:23 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: EXPIRED



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

__Cache hits__ can be reviewed in Nginx Plus Dashboard. In this case we will use __plus2__ node's Dashboard in http://http://192.168.56.7:8090/dashboard.html.

>NOTE: You can play a bit requesting the same URL and reviewing how __cache hit__ will grow as we execute requests.

![](./images/lab2_step6_1.jpg)

## Caching content for different clients with Nginx.

7 - Let's change a bit this behavior. We will change __proxy_cache_valid__ to maintain cached content for at least 1 minute and we added __$remote_addr__ to __proxy_cache_key__. This will prevent caching from different clients requesting the same content.

>__IMPORTANT NOTE: We are using \\\$ to avoid variable substitution using tee to create Nginx configuration files. Override "\\" characters before "$" if you edit these files by yourself without using _cat_ or _echo_.__
```
[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/picsum.conf

upstream picsum {
    server 10.1.1.5:8005;
}

proxy_cache_path /tmp/cache keys_zone=picsumCache:10m inactive=60m;

proxy_cache_min_uses 5;

proxy_cache_valid any 60s;

proxy_cache_key "\$remote_addr\$request_uri";

server {
    listen 8080 default_server;
    error_log /var/log/nginx/picsum.error.log info;
    access_log /var/log/nginx/picsum.access.log combined;
    status_zone picsum;

    location / {
        proxy_pass http://picsum;
        proxy_cache picsumCache;
        proxy_cache_methods GET;
        add_header X-Proxy-Cache \$upstream_cache_status;
    }
}
EOF
```

Reload NGINX
```
[plus2]$ sudo nginx -t

[plus2]$ sudo nginx -s reload
```


We can now execute a ___httpie___ from __docker__ host to verify how caching will show us content even when backend does not respond. This is a simulation and having long cache times is not a good practice. Caching parameters must follow your application behavior and content freshness.
```
[docker]$ date ; http http://192.168.56.7:8080/
jue jul 16 14:04:06 CEST 2020
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:04:06 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: EXPIRED



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

Now we stop __picsum__ container.
```
[docker]$ docker stop picsum
picsum
```

And we check again
```
[docker]$ date ; http http://192.168.56.7:8080/
jue jul 16 14:04:20 CEST 2020
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:04:20 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

We still have a valid response because image is cached and we haven't changed the request (same URL and remote client).
```
[docker]$ date ; http http://192.168.56.7:8080/
jue jul 16 14:04:22 CEST 2020
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:04:22 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

If we check now from browser, remote __client IP address__ will change and consequently, we will get an error.

![](./images/lab2_step7_1.jpg)


If we check again using ___httpie___ from __docker__ host after a minute, we will receive the same error. Cache was invalidated and content must be requested again. Due to __picsum__ backend is down, we get a __"502 Bad Gateway"__ error.
```
[docker]$ date ; http http://192.168.56.7:8080/
jue jul 16 14:06:18 CEST 2020
HTTP/1.1 502 Bad Gateway
Connection: keep-alive
Content-Length: 157
Content-Type: text/html
Date: Thu, 16 Jul 2020 12:06:19 GMT
Server: nginx/1.19.0

<html>
<head><title>502 Bad Gateway</title></head>
<body>
<center><h1>502 Bad Gateway</h1></center>
<hr><center>nginx/1.19.0</center>
</body>
</html>
```

10 - We will start again __picsum__ application's backend and learn how to purge cached content using Nginx's API.
```
[docker]$ docker start picsum
picsum
```

In this lab we learned how to provide cached content even if the applications backends are not responding.

## Purging cache content with Nginx.

11 - We now add __PURGUE__ method. We use a map to define its __default__ value to __0__.
```
[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/picsum.conf

upstream picsum {
    server 10.1.1.5:8005;
}

proxy_cache_path /tmp/cache keys_zone=picsumCache:10m inactive=60m;

proxy_cache_min_uses 5;

proxy_cache_valid any 60s;

proxy_cache_key "\$remote_addr\$request_uri";

map \$request_method \$purge_method {
    PURGE 1;
    default 0;
}

server {
    listen 8080 default_server;
    error_log /var/log/nginx/picsum.error.log info;
    access_log /var/log/nginx/picsum.access.log combined;
    status_zone picsum;

    location / {
        proxy_pass http://picsum;
        proxy_cache picsumCache;
        proxy_cache_purge \$purge_method;
        proxy_cache_methods GET;
        add_header X-Proxy-Cache \$upstream_cache_status;
    }
}
EOF
```

Reload NGINX
```
[plus2]$ sudo nginx -t

[plus2]$ sudo nginx -s reload
```


If request method is "PURGUE", __proxy_cache_purge__ will get value of __1__ and cache will be purgued.

11 - Let's verify this new behavior executing 3 requests to get cached content from __docker__ node.

We execute four requests.
```
[docker]$ http http://192.168.56.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:41:54 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: EXPIRED



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

Again
```
[docker]$ http http://192.168.56.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:42:25 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

And again
```
[docker]$ http http://192.168.56.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:42:26 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

And finally
```
[docker]$ http http://192.168.56.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:42:27 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

Now, we purge the cache using "PURGE" method. We will use __curl__ for example.
```
[docker]$ curl -X PURGE http://192.168.56.7:8080/
```

If we check again, cached content is not reached.
```
[docker]$ http http://192.168.56.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:41:54 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: EXPIRED



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

This allows us to manage cached content behavior using __PURGE method__ on our requests. 


