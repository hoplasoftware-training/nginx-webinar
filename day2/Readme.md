# Day 2 Workshops

>NOTE: Additional information and easy copy&pasting will be available in [http://dontpad.com/HoplaWorkshops](http://dontpad.com/HoplaWorkshops).

0 - Access environment and Join this workshop.

This lab will go through initial setup and add you the agility class.

***NOTE***

>*a prerequisite for this exercise is having an RDP client on your machine such as remote desktop connection.
   If you do not have one, please download one, Some examples are*
   >
   >*Remote desktop connection (macOS)
   https://apps.apple.com/us/app/microsoft-remote-desktop/*
   >
   >*Chrome browser RDP    https://remotedesktop.google.com/*

   ---

Follow these steps to complete this lab:

**Step 1 - Setting Up Lab Workstation**

1. Open your web browser
2. Navigate to https://udf.f5.com/courses
3. Login using your UDF credentials, you should have received an email from noreply@registration.udf.f5.com

![email](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/1email.png)

4. Upon first login, you will be prompted to change your password.

![password](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/2password.png)

5. Review and accept the terms for using UDF.

![3terms](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/3terms.png)

6. Locate the class "Nginx Plus  & integrating with BIG-IP" and click launch.

![4launch](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/4launch.png)

7. You will be given a overview the class, press join.

![5join](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/5join.png)

8. click on the 'deployment' tab in the top left, notice your lab environment is spinning up.

![7spinning](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/7spinning.png)

**Step 2 - RDP to Windows Jumphost**

In this exercise, we will connect to the Windows Jumphost.

1. Under the 'Systems' column, locate the 'Jumphost' block.

  ![8jumphost](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/8jumphost.png)


2. Click 'Access' -> 'RDP' and this will download a '.rdp' file to your local machine.

3. Once the RDP has downloaded, open the .rdp file and when prompted, select 'continue'.

4. When prompted for the login credentials, use username: user: password: user

  ![9rdp](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/9rdp.png)

5. You should now be in your windows Jumphost.

   ![windows](https://gitlab.com/hoplasoftware-training/nginx-webinar/-/raw/master/day1/images/windows.png)


---
NOTE: These labs can also be executed offline using Vagrant and Virtualbox virtual machines. A full deployment is provided using the same IP addresses mentioned in the cloud environment. You can deploy this environment in your laptop or PC if you meet following requirements:
- Working Internet connection.
- A minimum of 8GB of RAM, 4vCPU and 30GB of free disk space.
- Vagrant 2.2.7 or newer.
- Virtualbox 6.0 or newer.

Vagrant virtual environment should be deployed from __local-infra__ directory using ___vagrant up___ command. This will run 3 nodes by default:
- docker - 10.1.1.5 (internal only)/192.168.56.5 (host-to-guest)
- plus2 - 10.1.1.7 (internal only)/192.168.56.7 (host-to-guest)
- plus3 - 10.1.1.8 (internal only)/192.168.56.8 (host-to-guest)

>NOTE: You can find additional information in [Local Infra Readme](./local-infra/README.md)
---

## Prepare Environment for the labs

These are common steps that may be automated. Check if these configurations are already done in your environment to avoid future problems during the session.

>NOTE: Disabling the Firewall and SELinux can be deployed using __./scripts/prepare_node.sh__

- Disable Firewalld for these labs on all nodes (__plus2__, __plus3__ and __docker__).
```
[plus2/plus3/docker]$ sudo systemctl stop firewalld

[plus2/plus3/docker]$ sudo systemctl disable firewalld
```

- Disable SELinux for these labs on all nodes (__plus2__, __plus3__ and __docker__).
```
[plus2/plus3/docker]$ sudo getenforce
Enforcing

[plus2/plus3/docker]$ sudo setenforce 0

[plus2/plus3/docker]$ sudo getenforce
Permissive


[plus2/plus3/docker] sudo sed -i "s/SELINUX=.*/SELINUX=disabled/" /etc/sysconfig/selinux

```

If you are using Vagrant provided environment, we need to install Ngnix and Docker Engine. We provided you few scripts to automate this process.

- Install current Nginx release on __plus2__ and __plus3__ nodes. For this to work, you must download an NGINX Plus trial license in order to install the required Nginx Plus release. Once registered, download you __"nginx-repo.crt"__ and __"nginx-repo.key"__ to ___local-infra/scripts/nginx-repo/___ directory. These files will be used during installation process.
```
[plus2/plus3]$ cd /vagrant/scripts/
[plus2/plus3]$ ./install_and_upgrade.sh
```

- Deploy applications' backends on __docker__ node:
```
[docker]$ cd /vagrant/scripts/
[docker]$ ./deploy_app_backends.sh
```

```
[docker]$ cat <<EOF >deploy_app_backends.sh
#!/bin/bash

sudo systemctl restart docker

sudo docker rm -fv green-8001 blue-8002 red-8003 picsum > /dev/null 2>&1

echo -e "\n## Deploying GREEN application on port 8001"

sudo docker run -d --name green-8001 \
-e COLOR=green \
-p 8001:3000 \
-l color=green \
--restart=unless-stopped \
codegazers/colors:1.17


echo -e "\n## Deploying BLUE application on port 8002"

sudo docker run -d --name blue-8002 \
-e COLOR=blue \
-p 8002:3000 \
-l color=blue \
--restart=unless-stopped \
codegazers/colors:1.17


echo -e "\n## Deploying RED application on port 8003"

sudo docker run -d --name red-8003 \
-e COLOR=red \
-p 8003:3000 \
-l color=red \
--restart=unless-stopped \
codegazers/colors:1.17


echo -e "\n## Deploying PICSUM application on port 8005"

sudo docker run -d --name picsum \
-e MAX_NUMBER_OF_PICTURES=20 \
-e MAX_PICTURE_SOURCE_URL=https://picsum.photos/1024 \
-p 8005:80 \
--restart=unless-stopped \
bosix/test-picture-provider

EOF
```



---

# Labs

## Lab 1 - Persist Nginx dynamic configurations.

In this lab we will learn how to manage dynamic configurations using Nginx Web UI and its API without missing persistence.


1 - Enabling Dashboard on __plus2__ and __lab3__ nodes.
```
[plus2/plus3]$ cat <<EOF |sudo tee /etc/nginx/conf.d/dashboard.conf

server {
    listen 8090;
    location /api {
      limit_except GET {
        auth_basic "NGINX Plus API";
        auth_basic_user_file /etc/nginx/.API_passwd;
      }
      api   write=on;
      allow 192.168.56.0/24;
      allow 10.1.1.0/24;
      deny  all;
    }
    location = /dashboard.html {
        root   /usr/share/nginx/html;
    }
    location /swagger-ui {
        root   /usr/share/nginx/html;
    }
}
EOF
```

We generate admin credentials to perform Dashboard modifications.
```
[plus2/plus3]$ echo -n 'admin:'|sudo tee -a /etc/nginx/.API_passwd
[plus2/plus3]$ echo -n 'secret' | openssl passwd -apr1 -stdin|sudo tee -a /etc/nginx/.API_passwd
```


Reload NGINX
```
[plus2/plus3]$ sudo nginx -t

[plus2/plus3]$ sudo nginx -s reload
```
Dashboard for nodes __plus2__ and __plus3__ will be available on http://10.1.1.7:8090/dashboard.html (or http://192.168.56.7:8090/dashboard.html for internal network) and http://10.1.1.8:8090/dashboard.html (http://192.168.56.8:8090/dashboard.html), respectively.

>NOTE: If you are using Vagrant environments, Dashboard will be available on http://192.168.56.7:8090/dashboard.html and http://192.168.56.8:8090/dashboard.html, respecitevly.

2 - We will now add colors applications, distributed on different zones on __plus2__ node:
```
[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/green.conf
upstream green_servers {
  zone green_zone 64k;
  server 10.1.1.5:8001;
}

server {
  server_name green.nginx-udf.internal;
  listen 80;
    location / {
       proxy_pass http://green_servers;
    }
}
EOF
```
Same for ___blue___ and ___red___ backends

```
[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/blue.conf
upstream blue_servers {
  zone blue_zone 64k;
  server 10.1.1.5:8002;
}

server {
  server_name blue.nginx-udf.internal;
  listen 80;
    location / {
       proxy_pass http://blue_servers;
    }
}
EOF


[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/red.conf
upstream red_servers {
  zone red_zone 64k;
  server 10.1.1.5:8003;
}

server {
  server_name red.nginx-udf.internal;
  listen 80;
    location / {
       proxy_pass http://red_servers;
    }
}
EOF
```

3 - Review Dashboard and different applications' backend status.

![](./images/lab1_step3_1.jpg)


![](./images/lab1_step3_2.jpg)

4 - Let's scale up applications' backends adding more containers. Execute ___scale_app_backends.sh___ on __docker__ host.
```
[docker]$ ./scripts/scale_app_backends.sh
```

```
[docker]$ cat <<EOF >scale_app_backends.sh
#!/bin/bash -x

sudo docker rm -fv green-9001 blue-9002 red-9003 > /dev/null 2>&1

echo -e "\n## Deploying new GREEN application on port 9001"

sudo docker run -d --name green-9001 \
    -e COLOR=green \
    -p 9001:3000 \
    -l color=green \
    --restart=unless-stopped \
    codegazers/colors:1.17

echo -e "\n## Deploying BLUE application on port 9002"

sudo docker run -d --name blue-9002 \
    -e COLOR=blue \
    -p 9002:3000 \
    -l color=blue \
    --restart=unless-stopped \
    codegazers/colors:1.17

echo -e "\n## Deploying RED application on port 9003"

sudo docker run -d --name red-9003 \
    -e COLOR=red \
    -p 9003:3000 \
    -l color=red \
    --restart=unless-stopped \
    codegazers/colors:1.17


sudo docker rm -fv green-10001 blue-10002 red-10003 > /dev/null 2>&1

echo -e "\n## Deploying new GREEN application on port 10001"

sudo docker run -d --name green-10001 \
    -e COLOR=green \
    -p 10001:3000 \
    -l color=green \
    --restart=unless-stopped \
    codegazers/colors:1.17

echo -e "\n## Deploying BLUE application on port 10002"

sudo docker run -d --name blue-10002 \
    -e COLOR=blue \
    -p 10002:3000 \
    -l color=blue \
    --restart=unless-stopped \
    codegazers/colors:1.17

echo -e "\n## Deploying RED application on port 10003"

sudo docker run -d --name red-10003 \
    -e COLOR=red \
    -p 10003:3000 \
    -l color=red \
    --restart=unless-stopped \
    codegazers/colors:1.17


EOF
```



We can now filter by color:
- Green backends:
    ```
    [docker]$ docker container ls --filter "label=color=green" --format='{{.Label "color"}} {{.Ports}}'
    green 0.0.0.0:10001->3000/tcp
    green 0.0.0.0:9001->3000/tcp
    green 0.0.0.0:8001->3000/tcp
    ```

- Blue backends
    ```
    [docker]$ docker container ls --filter "label=color=blue" --format='{{.Label "color"}} {{.Ports}}'
    blue 0.0.0.0:10002->3000/tcp
    blue 0.0.0.0:9002->3000/tcp
    blue 0.0.0.0:8002->3000/tcp
    ```

- Red backends
    ```
    [docker]$ docker container ls --filter "label=color=red" --format='{{.Label "color"}} {{.Ports}}'
    red 0.0.0.0:10003->3000/tcp
    red 0.0.0.0:9003->3000/tcp
    red 0.0.0.0:8003->3000/tcp
    ```

> NOTE: We can filter all containers by color using defined "color" label:
>```
>[docker]$ docker container ls --filter "label=color" --format='{{.Label "color"}}\t{{.Ports}}'|sort
>blue   0.0.0.0:10002->3000/tcp
>blue   0.0.0.0:8002->3000/tcp
>blue   0.0.0.0:9002->3000/tcp
>green  0.0.0.0:10001->3000/tcp
>green  0.0.0.0:8001->3000/tcp
>green  0.0.0.0:9001->3000/tcp
>red    0.0.0.0:10003->3000/tcp
>red    0.0.0.0:8003->3000/tcp
>red    0.0.0.0:9003->3000/tcp
>```


5 - Let's add these new backends using Nginx Dashboard on __plus2__ node.

![](./images/lab1_step5_1.jpg)

![](./images/lab1_step5_2.jpg)

![](./images/lab1_step5_3.jpg)

![](./images/lab1_step5_4.jpg)

![](./images/lab1_step5_5.jpg)

![](./images/lab1_step5_6.jpg)

![](./images/lab1_step5_7.jpg)


6 - If we test __blue__ application now, we will reach 3 different backends:
>NOTE: You can use your web browser but remember to add "blue.nginx-udf.internal" defined host header. This will guide requests to __blue__ specific group of backends.

Using ___httpie___ or ___curl___ is easy to verify that issuing 3 requests we will reach 3 different backends.

```
[plus2]$ http  http://10.1.1.7:80/text host:blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Wed, 15 Jul 2020 17:08:44 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 167981170a43
CONTAINER_IP:  172.17.0.6
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 167981170a43
DATETIME: July 15th 2020, 5:08:44 pm
HEADERS:
        - host: "blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"
```

Again
```
[plus2]$ http  http://10.1.1.7:80/text host:blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Wed, 15 Jul 2020 17:08:47 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 8bd5a644a243
CONTAINER_IP:  172.17.0.4
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 8bd5a644a243
DATETIME: July 15th 2020, 5:08:47 pm
HEADERS:
        - host: "blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"


```

And again
```
[plus2]$ http  http://10.1.1.7:80/text host:blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Wed, 15 Jul 2020 17:08:48 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: c53c5e487258
CONTAINER_IP:  172.17.0.9
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): c53c5e487258
DATETIME: July 15th 2020, 5:08:48 pm
HEADERS:
        - host: "blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"


```

7 - Let's reload __plus2__ configuration now using ___nginx -s reload___ and verify again __blue__ backends:
```
[plus2]$ sudo nginx -s reload
```

Now, we send 3 requests again:
```
[plus2]$ http  http://10.1.1.7:80/text host:blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Wed, 15 Jul 2020 17:22:34 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: c53c5e487258
CONTAINER_IP:  172.17.0.9
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): c53c5e487258
DATETIME: July 15th 2020, 5:22:34 pm
HEADERS:
        - host: "blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"


```

Again
```
[plus2]$ http  http://10.1.1.7:80/text host:blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Wed, 15 Jul 2020 17:22:36 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: c53c5e487258
CONTAINER_IP:  172.17.0.9
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): c53c5e487258
DATETIME: July 15th 2020, 5:22:36 pm
HEADERS:
        - host: "blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"


```

And again
```
[plus2]$ http  http://10.1.1.7:80/text host:blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Wed, 15 Jul 2020 17:22:38 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: c53c5e487258
CONTAINER_IP:  172.17.0.9
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): c53c5e487258
DATETIME: July 15th 2020, 5:22:38 pm
HEADERS:
        - host: "blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"


```

All requests reached the same backend. In fact in __plus2__ dashboard we realize that all configurations were cleared.

![](./images/lab1_step7_1.jpg)


> ___All configurations created using API or Dashboard web UI will be cleared on Nginx reload unless we create persistent configurations.___

8 - Let's prepare our configuration to manage persistent changes using Nginx's Dashboard web UI or its API.

We will redefine __blue__ upstream block to use an external __state__ file.
```
[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/blue.conf
upstream blue_servers {
  zone blue_zone 64k;
  state /var/lib/nginx/state/appservers.conf;

}

server {
  server_name blue.nginx-udf.internal;
  listen 80;
    location / {
       proxy_pass http://blue_servers;
    }
}
EOF
```

We will now reload its configuration and review the platform.
```
[plus2]$ sudo nginx -s reload
```

We can review current upstreams under __blue_servers__ .
```
[plus2]$  http http://10.1.1.7:8090/api/6/http/upstreams/blue_servers
HTTP/1.1 200 OK
Cache-Control: no-cache
Connection: keep-alive
Content-Length: 57
Content-Type: application/json
Date: Wed, 15 Jul 2020 17:48:40 GMT
Expires: Thu, 01 Jan 1970 00:00:01 GMT
Server: nginx/1.19.0

{
    "keepalive": 0,
    "peers": [],
    "zombies": 0,
    "zone": "blue_zone"
}
```

![](./images/lab1_step8_1.jpg)

And now we can create a new backend.
```
[plus2]$ curl  --user admin:secret \
-X POST "http://10.1.1.7:8090/api/6/http/upstreams/blue_servers/servers/" \
-H "accept: application/json" \
-H "Content-Type: application/json" \
-d "{ \"server\": \"10.1.1.5:8002\", \
\"weight\": 1, \
\"max_conns\": 0, \
\"max_fails\": 1, \
\"fail_timeout\": \"10s\", \
\"slow_start\": \"0s\", \
\"route\": \"\", \
\"backup\": false, \
\"down\": false}"

{"id":9,"server":"10.1.1.5:8002","weight":1,"max_conns":0,"max_fails":1,"fail_timeout":"10s","slow_start":"0s","route":"","backup":false,"down":false}
```

Now we review again the servers grouped under __blue_servers__:
```
[plus2]]$ http http://10.1.1.7:8090/api/6/http/upstreams/blue_servers
HTTP/1.1 200 OK
Cache-Control: no-cache
Connection: keep-alive
Content-Length: 351
Content-Type: application/json
Date: Wed, 15 Jul 2020 18:14:15 GMT
Expires: Thu, 01 Jan 1970 00:00:01 GMT
Server: nginx/1.19.0

{
    "keepalive": 0,
    "peers": [
        {
            "active": 0,
            "backup": false,
            "downtime": 0,
            "fails": 0,
            "health_checks": {
                "checks": 0,
                "fails": 0,
                "unhealthy": 0
            },
            "id": 16,
            "name": "10.1.1.5:8002",
            "received": 0,
            "requests": 0,
            "responses": {
                "1xx": 0,
                "2xx": 0,
                "3xx": 0,
                "4xx": 0,
                "5xx": 0,
                "total": 0
            },
            "sent": 0,
            "server": "10.1.1.5:8002",
            "state": "up",
            "unavail": 0,
            "weight": 1
        }
    ],
    "zombies": 0,
    "zone": "blue_zone"
}
```

We can review on Nginx web UI Dashboard.

![](./images/lab1_step8_2.jpg)

9 - Now we add the other __blue__ backends.
```
[plus2]$ curl  --user admin:secret \
-X POST "http://10.1.1.7:8090/api/6/http/upstreams/blue_servers/servers/" \
-H "accept: application/json" \
-H "Content-Type: application/json" \
-d "{ \"server\": \"10.1.1.5:9002\", \
\"weight\": 1, \
\"max_conns\": 0, \
\"max_fails\": 1, \
\"fail_timeout\": \"10s\", \
\"slow_start\": \"0s\", \
\"route\": \"\", \
\"backup\": false, \
\"down\": false}"

{"id":10,"server":"10.1.1.5:9002","weight":1,"max_conns":0,"max_fails":1,"fail_timeout":"10s","slow_start":"0s","route":"","backup":false,"down":false}

[plus2]$ curl  --user admin:secret \
-X POST "http://10.1.1.7:8090/api/6/http/upstreams/blue_servers/servers/" \
-H "accept: application/json" \
-H "Content-Type: application/json" \
-d "{ \"server\": \"10.1.1.5:10002\", \
\"weight\": 1, \
\"max_conns\": 0, \
\"max_fails\": 1, \
\"fail_timeout\": \"10s\", \
\"slow_start\": \"0s\", \
\"route\": \"\", \
\"backup\": false, \
\"down\": false}"

{"id":11,"server":"10.1.1.5:10002","weight":1,"max_conns":0,"max_fails":1,"fail_timeout":"10s","slow_start":"0s","route":"","backup":false,"down":false}
```

All __blue__ backends appear on Nginx web UI Dashboard.

![](./images/lab1_step9_1.jpg)


We can check now the provided service accesing __blue.nginx-udf.internal__ using __plus2__ host IP address and __"host:blue.nginx-udf.internal"__ header with ___httpie___:
```
[plus2]$ http  http://10.1.1.7:80/text host:blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Wed, 15 Jul 2020 18:21:01 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: c53c5e487258
CONTAINER_IP:  172.17.0.9
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): c53c5e487258
DATETIME: July 15th 2020, 6:21:01 pm
HEADERS:
        - host: "blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"


```

Again
```
[plus2]$ http  http://10.1.1.7:80/text host:blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Wed, 15 Jul 2020 18:21:03 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 8bd5a644a243
CONTAINER_IP:  172.17.0.4
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 8bd5a644a243
DATETIME: July 15th 2020, 6:21:03 pm
HEADERS:
        - host: "blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"


```

And again
```
[plus2]$ http  http://10.1.1.7:80/text host:blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Wed, 15 Jul 2020 18:21:04 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 167981170a43
CONTAINER_IP:  172.17.0.6
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 167981170a43
DATETIME: July 15th 2020, 6:21:04 pm
HEADERS:
        - host: "blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"
```


10 - Let's reload again Nginx configuration:
```
[plus2]$ sudo nginx -s reload
```

All backends are still present and __blue__ application is working.

![](./images/lab1_step10_1.jpg)

Using ___httpie___:

```
[plus2]$ http  http://10.1.1.7:80/text host:blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Wed, 15 Jul 2020 18:27:43 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: c53c5e487258
CONTAINER_IP:  172.17.0.9
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): c53c5e487258
DATETIME: July 15th 2020, 6:27:43 pm
HEADERS:
        - host: "blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"


```

Again
```
[plus2]$ http  http://10.1.1.7:80/text host:blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Wed, 15 Jul 2020 18:27:44 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 8bd5a644a243
CONTAINER_IP:  172.17.0.4
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 8bd5a644a243
DATETIME: July 15th 2020, 6:27:44 pm
HEADERS:
        - host: "blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"


```

And again
```
[plus2]$ http  http://10.1.1.7:80/text host:blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Wed, 15 Jul 2020 18:27:45 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 167981170a43
CONTAINER_IP:  172.17.0.6
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 167981170a43
DATETIME: July 15th 2020, 6:27:45 pm
HEADERS:
        - host: "blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"

```

11 - We can finally review the content of __blue__ upstream block servers:
```
[plus2]$ sudo cat /var/lib/nginx/state/appservers.conf
server 10.1.1.5:8002;
server 10.1.1.5:9002;
server 10.1.1.5:10002;
```

> ___These persistent files should not be manipulated. Don not edit manually these files, Nginx formatting and matching can break normal behavior.___


This lab teached us how to implement persistent configurations using Nginx's API or its Dashboard. As we previously learned, all configurations made using these methods will be available only on memory unless we specify an external state file.




---

## Lab 2 - Caching content with Nginx.

In this lab we will learn how to provide content caching using Nginx.


1 - Configure Picsum application backend on __plus2__ and __lab3__ nodes. This application will show us a different picture on each request.
```
[plus2/plus3]$ cat <<EOF |sudo tee /etc/nginx/conf.d/picsum.conf

upstream picsum {
    server 10.1.1.5:8005;
}

server {
    listen 8080 default_server;
    error_log /var/log/nginx/picsum.error.log info;
    access_log /var/log/nginx/picsum.access.log combined;
    status_zone picsum;

    location / {
        proxy_pass http://picsum;
    }
}
EOF
```

Reload NGINX
```
[plus2/plus3]$ sudo nginx -t

[plus2/plus3]$ sudo nginx -s reload
```

We can use our browser to obtain images on http://10.1.1.7:8080 and http://10.1.1.8:8080.

>NOTE: Verify accesses to http://192.168.56.7:8080/ and http://192.168.56.8:8080/ using your web browser if you are running provided Vagrant environment.

If you refresh your browser, you will get a new image.

![](./images/lab2_step1_1.jpg)

![](./images/lab2_step1_2.jpg)

![](./images/lab2_step1_3.jpg)

Let's configure caching on __plus2__ node.

2 - Configure caching for Picsum application backend on __plus2__ node.

>__IMPORTANT NOTE: We are using \\\$ to avoid variable substitution using tee to create Nginx configuration files. Override "\\" characters before "$" if you edit these files by yourself without using _cat_ or _echo_.__
```
[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/picsum.conf

upstream picsum {
    server 10.1.1.5:8005;
}

proxy_cache_path /tmp/cache keys_zone=picsumCache:10m inactive=60m;

proxy_cache_min_uses 5;

proxy_cache_valid any 10s;

proxy_cache_key "\$request_uri";

server {
    listen 8080 default_server;
    error_log /var/log/nginx/picsum.error.log info;
    access_log /var/log/nginx/picsum.access.log combined;
    status_zone picsum;

    location / {
        proxy_pass http://picsum;
        proxy_cache picsumCache;
        add_header X-Proxy-Cache \$upstream_cache_status;
    }
}
EOF
```

Reload NGINX
```
[plus2]$ sudo nginx -t

[plus2]$ sudo nginx -s reload
```

3 - Let's verify again this new behavior using __plus2__ published application on port 8080. We can use our browser to obtain images on http://10.1.1.7:8080.

>NOTE: Verify accesses to http://192.168.56.7:8080/ using your web browser if you are running provided Vagrant environment.

![](./images/lab2_step2_1.jpg)

If we refresh in less than 10 seconds, we will obtain the same image because it is cached. Cache will be invalid in 10 seconds (__proxy_cache_valid any 10s__).
![](./images/lab2_step2_2.jpg)

If we wait more than 10 seconds, and refresh again, we will get a different image.

![](./images/lab2_step2_5.jpg)

We have to notice that __plus3__ node does not work the same. We have not configured image caching on that node hence refreshing will show us different pictures. We can verify this with our browser to obtain images on http://10.1.1.8:8080.

>NOTE: Verify accesses to http://192.168.56.8:8080/ using your web browser if you are running provided Vagrant environment.

![](./images/lab2_step2_3.jpg)

Now we refresh and get a different image.

![](./images/lab2_step2_4.jpg)

5 - We can now take a look at __/tmp/cache__ directory on __plus2__ node.
```
[plus2$ sudo ls -lRt /tmp/cache/
/tmp/cache/:
total 220
-rw-------. 1 nginx nginx 223691 jul 16 11:53 d41d8cd98f00b204e9800998ecf8427e
```

We only have one file because we requested just one url, "/", and from only one client. We configured __proxy_cache_key__ variable to use __"$request_uri"__. Therefore, cache will be evaluated on each requested URI.


As we change backends URL, images will also change. This is due to that caching key is configured to match only requested URI. We can verify this behavior with our browser using http://10.1.1.7:8080/test.

>NOTE: Verify accesses to http://192.168.56.7:8080/test using your web browser if you are running provided Vagrant environment.

![](./images/lab2_step2_7.jpg)

If we test again in less than 10 seconds using a new URI, we get a new image.

![](./images/lab2_step2_8.jpg)

We can verify this behavior with our browser using http://10.1.1.7:8080/test/1.

>NOTE: Verify accesses to http://192.168.56.7:8080/test/1 using your web browser if you are running provided Vagrant environment.

But if we refresh this new URL, we get the cached image.

![](./images/lab2_step2_9.jpg)


6 - We can use ___http___ instead of web browser to verify headers with cache information. Let's try a couple of times. We should be able to use any node in this lab to execute these tests.

In this case we don't care about images, we will take alook at __X-Proxy-Cache__ header. We will execute ____http http://10.1.1.7:8080/____ four times.
Try to execute them fast, don't expend more than 10 seconds between executions to avoid cache invalidation.
```
[plus3]$ http http://10.1.1.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 10:57:37 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: EXPIRED



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

Again
```
[plus3]$ http http://10.1.1.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 10:57:39 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

And again
```
[plus3]$ http http://10.1.1.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 10:57:40 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

And finally
```
[plus3]$ http http://10.1.1.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 10:57:42 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

Notice that first request get a "X-Proxy-Cache: EXPIRED" line. This is due to this is the first time we make this request. Nginx will load cached data and use it on subsequent requests. That's why we get "X-Proxy-Cache: HIT".

If we start again this sequence of executions, cache will be invalid again because this request is executed after 10 seconds.

```
[plus3]$ http http://10.1.1.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 11:04:23 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: EXPIRED



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

Cache hits can be reviewed in Nginx Plus Dashboard. In this case we will use __plus2__ node's Dashboard in http://10.1.1.7:8090/dashboard.html.

>NOTE: Verify accesses to http://192.168.56.7:8080/dashboard.html using your web browser if you are running provided Vagrant environment.

![](./images/lab2_step6_1.jpg)



7 - Let's change a bit this behavior. We will change __proxy_cache_valid__ to maintain cached content for at least 1 minute and we added __$remote_addr__ to __proxy_cache_key__. This will prevent caching from different clients requesting the same content.

>__IMPORTANT NOTE: We are using \\\$ to avoid variable substitution using tee to create Nginx configuration files. Override "\\" characters before "$" if you edit these files by yourself without using _cat_ or _echo_.__
```
[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/picsum.conf

upstream picsum {
    server 10.1.1.5:8005;
}

proxy_cache_path /tmp/cache keys_zone=picsumCache:10m inactive=60m;

proxy_cache_min_uses 5;

proxy_cache_valid any 60s;

proxy_cache_key "\$remote_addr\$request_uri";

server {
    listen 8080 default_server;
    error_log /var/log/nginx/picsum.error.log info;
    access_log /var/log/nginx/picsum.access.log combined;
    status_zone picsum;

    location / {
        proxy_pass http://picsum;
        proxy_cache picsumCache;
        proxy_cache_methods GET;
        add_header X-Proxy-Cache \$upstream_cache_status;
    }
}
EOF
```

Reload NGINX
```
[plus2]$ sudo nginx -t

[plus2]$ sudo nginx -s reload
```


We can now execute a ___httpie___ from __docker__ host to verify how caching will show us content even when backend does not respond. This is a simulation and having long cache times is not a good practice. Caching parameters must follow your application behavior and content freshness.
```
[docker]$ date ; http http://10.1.1.7:8080/
jue jul 16 14:04:06 CEST 2020
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:04:06 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: EXPIRED



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

Now we stop __picsum__ container.
```
[docker]$ docker stop picsum
picsum
```

And we check again
```
[docker]$ date ; http http://10.1.1.7:8080/
jue jul 16 14:04:20 CEST 2020
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:04:20 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

We still have a valid response because image is cached and we haven't changed the request (same URL and remote client).
```
[docker]$ date ; http http://10.1.1.7:8080/
jue jul 16 14:04:22 CEST 2020
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:04:22 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

If we check now from browser, remote client IP Address will change and consequently, we will get an error.

![](./images/lab2_step7_1.jpg)


If we check again using ___httpie___ from __docker__ host after a minute, we will receive the same error. Cache was invalidated and content must be requested again. Due to __picsum__ backend is down, we get a __"502 Bad Gateway"__ error.
```
[docker]$ date ; http http://10.1.1.7:8080/
jue jul 16 14:06:18 CEST 2020
HTTP/1.1 502 Bad Gateway
Connection: keep-alive
Content-Length: 157
Content-Type: text/html
Date: Thu, 16 Jul 2020 12:06:19 GMT
Server: nginx/1.19.0

<html>
<head><title>502 Bad Gateway</title></head>
<body>
<center><h1>502 Bad Gateway</h1></center>
<hr><center>nginx/1.19.0</center>
</body>
</html>
```

10 - We will start again __picsum__ application's backend and learn how to purge cached content using Nginx's API.
```
[docker]$ docker start picsum
picsum
```

We now add __PURGUE__ method. We use a map to define its __default__ value to __0__.
```
[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/picsum.conf

upstream picsum {
    server 10.1.1.5:8005;
}

proxy_cache_path /tmp/cache keys_zone=picsumCache:10m inactive=60m;

proxy_cache_min_uses 5;

proxy_cache_valid any 60s;

proxy_cache_key "\$remote_addr\$request_uri";

map \$request_method \$purge_method {
    PURGE 1;
    default 0;
}

server {
    listen 8080 default_server;
    error_log /var/log/nginx/picsum.error.log info;
    access_log /var/log/nginx/picsum.access.log combined;
    status_zone picsum;

    location / {
        proxy_pass http://picsum;
        proxy_cache picsumCache;
        proxy_cache_purge \$purge_method;
        proxy_cache_methods GET;
        add_header X-Proxy-Cache \$upstream_cache_status;
    }
}
EOF
```

Reload NGINX
```
[plus2]$ sudo nginx -t

[plus2]$ sudo nginx -s reload
```


If request method is "PURGUE", __proxy_cache_purge__ will get value of __1__ and cache will be purgued.

11 - Let's verify this new behavior executing 3 requests to get cached content from __docker__ node.

We execute four requests.
```
[docker]$ http http://10.1.1.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:41:54 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: EXPIRED



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

Again
```
[docker]$ http http://10.1.1.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:42:25 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

And again
```
[docker]$ http http://10.1.1.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:42:26 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

And finally
```
[docker]$ http http://10.1.1.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:42:27 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: HIT



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

Now, we purge the cache using "PURGE" method. We will use __curl__ for example.
```
[docker]$ curl -X PURGE -D -s -o /dev/null "http://10.1.1.7:8080/"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 16166    0 16166    0     0  5838k      0 --:--:-- --:--:-- --:--:-- 7893k

```

If we check again content has expired.
```
[docker]$ http http://10.1.1.7:8080/
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: image/jpeg
Date: Thu, 16 Jul 2020 12:41:54 GMT
Server: nginx/1.19.0
Transfer-Encoding: chunked
X-Proxy-Cache: EXPIRED



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

In this lab we learned how to provide cached content even if the applications backends are not responding.

---

## LAB 3 -

In this lab we are going to work with persistence of sessions. NGINX Plus supports three session persistence methods. The methods are set with the sticky directive, in this case we will use the Sticky Cookie method. Session persistence with NGINX Open Source, is only available using hash or ip_hash directives.

1 - Sticky Cookie with path parameter.
We are going to deploy a configuration in which for the upstream blue we are going to set a session persistence via cookie. (sticky cookie).
To do this we will use the sticky cookie directive with the following parameters:
- Cookie name: blue_cookie
- Path for which the cookie is set: /text


```
[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/persistent-blue.conf
upstream persistent_blue_servers {
  zone blue_zone 64k;
  sticky cookie blue_cookie path=/text;
  server 10.1.1.5:8002;
  server 10.1.1.5:9002;
  server 10.1.1.5:10002;
}

server {
  server_name persistent-blue.nginx-udf.internal;
  listen 80;
    location / {
       proxy_pass http://persistent_blue_servers;
    }
}
EOF
```
Let's reload __plus2__ configuration now using ___nginx -s reload___ and verify again __blue__ backends:
```
[plus2]$ sudo nginx -s reload
```

Ensure __docker__ node has all blue backends running (we have seen this on LAB1 step 4).

2 - Now we are able to test our sticky session configuration. We can use either our web browser or ___httpie___. Using ___httpie___ is easier, we have described both methods.

- Using your web browser:
>NOTE: You have to add "persistent-blue.nginx-udf" to your client __hosts__ file or use an already included __Host Header__ for this service name.
To check the session persistence by means of a cookie, open 2 tabs in the web browser of the Windows node.
In one we will open the url: http://10.1.1.7/html (or http://192.168.56.7/html if you used vagrant deployment), and in the other one http://10.1.1.7/text (or http://192.168.56.7/txt).
If we refresh the page several times in the /html tab we'll see that each request is handled by a different backend.

![](./images/lab3_step1_1.png)

However if we do the same on the /text tab we'll see that the request will always be served by the same backend server. This is because with the sticky cookie configuration we have done, this configuration only affects the /text path.

![](./images/lab3_step1_2.png)

If we delete the cookies from the /text tab and reload the web page we'll see that the request is attended by a new backend server and from that moment on, our requests, will always be attended by that backend server.
To do this we will delete the cookie from the developer tools view of the web browser.

![](./images/lab3_step1_3.png)

Select "Application" from the options on the top menu bar.
From this view, and if we refresh the page several times, we can check that it will always be the same backend that responds to our requests, unless the backend fails or we delete the cookie.

![](./images/lab3_step1_4.png)

Let's delete the cookie set from the browser tab we're on. To do this we carry out the following steps:

![](./images/lab3_step1_5.png)

![](./images/lab3_step1_6.png)

Once the cookie has been deleted, we refresh the page several times and check that the backend that serves our requests has changed, as well as the cookie.

![](./images/lab3_step1_7.png)

- Using ___httpie___:

We can use ___httpie___ directly, executing 3 requests for example to http://10.1.1.7/text  using __"persistent-blue.nginx-udf.internal"__ as __Host Header__:
```
[docker]$ http  http://10.1.1.7/text host:persistent-blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Mon, 20 Jul 2020 10:40:11 GMT
Server: nginx/1.19.0
Set-Cookie: blue_cookie=60b8660fa744dfb9e698c50beedd0a30; path=/text
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: cbca826cd4ef
CONTAINER_IP:  172.17.0.10
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): cbca826cd4ef
DATETIME: July 20th 2020, 10:40:11 am
HEADERS:
        - host: "persistent_blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"



[docker]$ http  http://10.1.1.7/text host:persistent-blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Mon, 20 Jul 2020 10:40:13 GMT
Server: nginx/1.19.0
Set-Cookie: blue_cookie=64738dd22d1e2089e68e3d5a7ba7fc73; path=/text
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 6fbd0aff58d7
CONTAINER_IP:  172.17.0.7
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 6fbd0aff58d7
DATETIME: July 20th 2020, 10:40:13 am
HEADERS:
        - host: "persistent_blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"



[docker]$ http  http://10.1.1.7/text host:persistent-blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Mon, 20 Jul 2020 10:40:14 GMT
Server: nginx/1.19.0
Set-Cookie: blue_cookie=d84ad138dbbcea3b5a856fecdb7b85c3; path=/text
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 7870e58068d8
CONTAINER_IP:  172.17.0.3
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 7870e58068d8
DATETIME: July 20th 2020, 10:40:14 am
HEADERS:
        - host: "persistent_blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"



```

Notice that each request went to a different backend. This is due to that by default __httpie__ does not reuse the same HTTP session. We will add __--session__ argument with a session name in our requests and test again 3 times:
```
[docker]$ http --session blue http://10.1.1.7/text host:persistent-blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Mon, 20 Jul 2020 10:53:17 GMT
Server: nginx/1.19.0
Set-Cookie: blue_cookie=64738dd22d1e2089e68e3d5a7ba7fc73; path=/text
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 6fbd0aff58d7
CONTAINER_IP:  172.17.0.7
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 6fbd0aff58d7
DATETIME: July 20th 2020, 10:53:17 am
HEADERS:
        - host: "persistent_blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"
        - cookie: "blue_cookie=64738dd22d1e2089e68e3d5a7ba7fc73"


[docker]$ http --session blue http://10.1.1.7/text host:persistent-blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Mon, 20 Jul 2020 10:53:19 GMT
Server: nginx/1.19.0
Set-Cookie: blue_cookie=64738dd22d1e2089e68e3d5a7ba7fc73; path=/text
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 6fbd0aff58d7
CONTAINER_IP:  172.17.0.7
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 6fbd0aff58d7
DATETIME: July 20th 2020, 10:53:19 am
HEADERS:
        - host: "persistent_blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"
        - cookie: "blue_cookie=64738dd22d1e2089e68e3d5a7ba7fc73"


[docker]$ http --session blue http://10.1.1.7/text host:persistent-blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Mon, 20 Jul 2020 10:53:19 GMT
Server: nginx/1.19.0
Set-Cookie: blue_cookie=64738dd22d1e2089e68e3d5a7ba7fc73; path=/text
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 6fbd0aff58d7
CONTAINER_IP:  172.17.0.7
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 6fbd0aff58d7
DATETIME: July 20th 2020, 10:53:19 am
HEADERS:
        - host: "persistent_blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"
        - cookie: "blue_cookie=64738dd22d1e2089e68e3d5a7ba7fc73"

```

This time all requests were made against one backend. Therefore, session persistency was applied. We also notice the value of the cookie assigned to this session.

>NOTE: ___Httpie__ stores sessions inside user's home following this pattern by default, ~/.httpie/sessions/<host>/<name>.json. In this example:
>```
>[vagrant@docker ~]$ ls -lart ~/.httpie/sessions/persistent-blue.nginx-udf.internal/
>total 4
>drwx------. 4 vagrant vagrant  73 jul 20 12:33 ..
>drwx------. 2 vagrant vagrant  23 jul 20 12:33 .
>-rw-rw-r--. 1 vagrant vagrant 538 jul 20 12:57 blue.json
>```
>
>Removing this file will clear out used session.


Let's move forward and prepare another Sticky Cookie lab adding expiring time.


3 - As par of this lab, we will change default session behaviour adding expiration time; that is, the time of validity of the cookie.
Once this time has expired, the next request will be attended by another backend, with another cookie and with the validity time we have indicated.
In this lab we will use 20 seconds as __expires__ argument.

```
[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/persistent-blue.conf
upstream persistent_blue_servers {
  zone blue_zone 64k;
  sticky cookie blue_cookie expires=20s path=/text;
  server 10.1.1.5:8002;
  server 10.1.1.5:9002;
  server 10.1.1.5:10002;
}

server {
  server_name persistent-blue.nginx-udf.internal;
  listen 80;
    location / {
       proxy_pass http://persistent_blue_servers;
    }
}
EOF
```
Let's reload __plus2__ configuration now using ___nginx -s reload___ and verify again __blue__ backends:
```
[plus2]$ sudo nginx -s reload
```

4 - Now we are able to test our sticky session configuration. We can use either our web browser or ___httpie___. Using ___httpie___ is easier, we have described both methods.

- Using your web browser:
>NOTE: You have to add "persistent-blue.nginx-udf" to your client __hosts__ file or use an already included __Host Header__ for this service name.
To check this new configuration we will open a tab in the web browser and go to the url http://10.1.1.7/text.
After reloading the page several times we'll see that, as in the previous lab, the backend that handles the requests is the same, as we've set the session permanence using a cookie and that it applies to the /text path.
However, this time we'll also look at the value of DATETIME, since after 20 seconds the cookie will expire and then our requests will be attended by another backend for another 20 seconds.

![](./images/lab3_step2_1.png)

Again we will open the developer tools on the website.

![](./images/lab3_step2_2.png)

Select "Application" from the options on the top menu bar.
Now, with this view, we can see both the backend that attends our requests, the cookie generated, and the expiration time of the cookie.
By refreshing the page, taking into account the cookie expiration time, we can see the change of backend, the cookie and the cookie expiration time every 20 seconds.

![](./images/lab3_step2_3.png)


- Using ___httpie___:
We can use ___httpie___ directly, executing 3 requests for example to http://10.1.1.7/text  using __"persistent-blue.nginx-udf.internal"__ as __Host Header__:

```
[vagrant@docker ~]$ http --session blue http://10.1.1.7/text host:persistent-blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Mon, 20 Jul 2020 11:18:35 GMT
Server: nginx/1.19.0
Set-Cookie: blue_cookie=d84ad138dbbcea3b5a856fecdb7b85c3; expires=Mon, 20-Jul-20 11:18:55 GMT; max-age=20; path=/text
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 7870e58068d8
CONTAINER_IP:  172.17.0.3
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 7870e58068d8
DATETIME: July 20th 2020, 11:18:35 am
HEADERS:
        - host: "persistent_blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"



[vagrant@docker ~]$ http --session blue http://10.1.1.7/text host:persistent-blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Mon, 20 Jul 2020 11:18:36 GMT
Server: nginx/1.19.0
Set-Cookie: blue_cookie=d84ad138dbbcea3b5a856fecdb7b85c3; expires=Mon, 20-Jul-20 11:18:56 GMT; max-age=20; path=/text
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 7870e58068d8
CONTAINER_IP:  172.17.0.3
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 7870e58068d8
DATETIME: July 20th 2020, 11:18:36 am
HEADERS:
        - host: "persistent_blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"
        - cookie: "blue_cookie=d84ad138dbbcea3b5a856fecdb7b85c3"



[vagrant@docker ~]$ http --session blue http://10.1.1.7/text host:persistent-blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Mon, 20 Jul 2020 11:18:38 GMT
Server: nginx/1.19.0
Set-Cookie: blue_cookie=d84ad138dbbcea3b5a856fecdb7b85c3; expires=Mon, 20-Jul-20 11:18:58 GMT; max-age=20; path=/text
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: 7870e58068d8
CONTAINER_IP:  172.17.0.3
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): 7870e58068d8
DATETIME: July 20th 2020, 11:18:38 am
HEADERS:
        - host: "persistent_blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"
        - cookie: "blue_cookie=d84ad138dbbcea3b5a856fecdb7b85c3"


```

Notice that first request didn't have a __Cookie__ associated. Then, all subsequent requests will use previously associated __Cookie__ to interact with the backends.

If we wait more than 20 seconds and execute a new request, __Cookie__ management starts again and same rules apply.
```
[vagrant@docker ~]$ http --session blue http://10.1.1.7/text host:persistent-blue.nginx-udf.internal
HTTP/1.1 200 OK
Connection: keep-alive
Date: Mon, 20 Jul 2020 11:21:28 GMT
Server: nginx/1.19.0
Set-Cookie: blue_cookie=60b8660fa744dfb9e698c50beedd0a30; expires=Mon, 20-Jul-20 11:21:48 GMT; max-age=20; path=/text
Transfer-Encoding: chunked

APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: cbca826cd4ef
CONTAINER_IP:  172.17.0.10
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): cbca826cd4ef
DATETIME: July 20th 2020, 11:21:28 am
HEADERS:
        - host: "persistent_blue_servers"
        - connection: "close"
        - accept-encoding: "gzip, deflate"
        - accept: "*/*"
        - user-agent: "HTTPie/0.9.4"



```

This lab shows us how to manage persistence associated with users or sessions interaction. In the next Lab we will learn how to synchronize persistence across cluster nodes.



---

## Lab 4
In this lab we will learn how to sync configurations and states between nodes in Nginx clusters (set of Nginx nodes).

1 - First we will configure rate limits for some application content we will configure on __plus2__ and __plus3__ nodes a new service to manage "green" requests. We will listen on port __1080__ requests sent to __"green.nginx-udf.internal"__.
```
[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/green_limited.conf
limit_req_zone \$binary_remote_addr zone=green_zone_limit:10m rate=1r/m;

# green_servers should be already configured on lab1 on plus2
#upstream green_servers {
#  zone green_servers 64k;
#  server 10.1.1.5:8001;
#}

server {
  listen 1080;
    location /text {
       limit_req zone=green_zone_limit;
       proxy_pass http://green_servers;
    }
}
EOF
```

Reload NGINX
```
[plus2]$ sudo nginx -t

[plus2]$ sudo nginx -s reload
```


If we launch 10 requests to defined URI, we will notice that we only get the first one sucessfully.
```
[docker]$ for I in {1..10};do date; curl --fail http://10.1.1.7:1080/text;done
vie jul 17 14:42:16 CEST 2020
APP_VERSION: 1.20
COLOR: green
CONTAINER_NAME: e861a683da90
CONTAINER_IP:  172.17.0.2
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): e861a683da90
DATETIME: July 17th 2020, 12:42:16 pm
HEADERS:
        - host: "green_servers"
        - connection: "close"
        - user-agent: "curl/7.29.0"
        - accept: "*/*"

vie jul 17 14:42:16 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
vie jul 17 14:42:16 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
vie jul 17 14:42:16 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
vie jul 17 14:42:16 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
vie jul 17 14:42:16 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
vie jul 17 14:42:16 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
vie jul 17 14:42:16 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
vie jul 17 14:42:16 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
vie jul 17 14:42:16 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
```

Rate limit will only accept 1 request per minute. therefore we have to wait one minute to be able to get a new sucessfully response.

![](./images/lab4_step1_1.jpg)


2 - Let's configure this service on __plus3__ too. Notice that configurations are different due to __"green_servers"__ upstream definition, already present on __plus2__.
```
[plus3]$ cat <<EOF |sudo tee /etc/nginx/conf.d/green_limited.conf
limit_req_zone \$binary_remote_addr zone=green_zone_limit:10m rate=1r/m;

# green_servers should be already configured on lab1 on plus2

upstream green_servers {
  zone green_servers 64k;
  server 10.1.1.5:8001;
}

server {
  listen 1080;
    location /text {
       limit_req zone=green_zone_limit;
       proxy_pass http://green_servers;
    }
}
EOF
```

Reload NGINX
```
[plus3]$ sudo nginx -t

[plus3]$ sudo nginx -s reload
```


3 - If we now try both services, configured on __plus2__ and __plus3__, we notice that they work isolated.

- Testing __lab2 green__ service http://10.1.1.7:1080/text:
```
[docker]$ for I in {1..3};do date; curl --fail http://10.1.1.7:1080/text;done
vie jul 17 14:57:01 CEST 2020
APP_VERSION: 1.20
COLOR: green
CONTAINER_NAME: e861a683da90
CONTAINER_IP:  172.17.0.2
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): e861a683da90
DATETIME: July 17th 2020, 12:57:01 pm
HEADERS:
        - host: "green_servers"
        - connection: "close"
        - user-agent: "curl/7.29.0"
        - accept: "*/*"

vie jul 17 14:57:01 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
vie jul 17 14:57:01 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
```

- And the same on __lab3__, http://10.1.1.8:1080/text:

```
[docker]$ for I in {1..3};do date; curl --fail http://10.1.1.8:1080/text;done
vie jul 17 14:57:06 CEST 2020
APP_VERSION: 1.20
COLOR: green
CONTAINER_NAME: e861a683da90
CONTAINER_IP:  172.17.0.2
CLIENT_IP: ::ffff:10.1.1.8
CONTAINER_ARCH: linux
HOSTNAME (from environment): e861a683da90
DATETIME: July 17th 2020, 12:57:06 pm
HEADERS:
        - host: "green_servers"
        - connection: "close"
        - user-agent: "curl/7.29.0"
        - accept: "*/*"

vie jul 17 14:57:06 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
vie jul 17 14:57:06 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
```

Both servers admit only 1 request per minute, but request counter is not shared. Let's move on and make this value available on both nodes and the same time. This way both nodes will know how many requests were executed from one client IP address.


4 - To enable __Zone Synchronization__ we need to configure a new _streaming_ service.

First we modify current general ___nginx.conf___ configuration file. In this lab we will simply add an __"include"__ directive to add an external file (as we are already doing with _http_ configurations).
On __plus2__ and __plus3__ Nginx servers we add this line at the end of ___/etc/nginx/nginx.conf___:
```
[plus2/plus3]$ cat <<EOF |sudo tee -a /etc/nginx/nginx.conf
stream {
  include /etc/nginx/conf.d/*.stream;
}
EOF
```

And now we can create a new configuration for zone syncing on both nodes.
```
[plus2/plus3]$ cat <<EOF |sudo tee /etc/nginx/conf.d/zone_sync.stream;
server {
    listen 9000;
    zone_sync;
    zone_sync_server 10.1.1.7:9000;
    zone_sync_server 10.1.1.8:9000;
}
EOF
```

We can check that everything is fine, although we haven't configured any zone for syncing yet.

Reload NGINX
```
[plus2/plus3]$ sudo nginx -t

[plus2/plus3]$ sudo nginx -s reload
```

>NOTE: We will get a warning message because there isn't any zone confired yet.
> nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
> nginx: [warn] no zones to sync for server in /etc/nginx/conf.d/zone_sync.stream:1
> nginx: configuration file /etc/nginx/nginx.conf test is successful



5 - We will now configure zone syncing for __"green_zone_limit"__.

On __plus2__ node:

```
[plus2]$ cat <<EOF |sudo tee /etc/nginx/conf.d/green_limited.conf
limit_req_zone \$binary_remote_addr zone=green_zone_limit:10m rate=1r/m sync;

# green_servers should be already configured on lab1 on plus2
#upstream green_servers {
#  zone green_servers 64k;
#  server 10.1.1.5:8001;
#}

server {
  listen 1080;
    location /text {
       limit_req zone=green_zone_limit;
       proxy_pass http://green_servers;
    }
}
EOF
```

On __plus3__ node (notice that configurations are different due to __"green_servers"__ upstream definition, already present on __plus2__):

```
[plus3]$ cat <<EOF |sudo tee /etc/nginx/conf.d/green_limited.conf
limit_req_zone \$binary_remote_addr zone=green_zone_limit:10m rate=1r/m sync;

# green_servers should be already configured on lab1 on plus2

upstream green_servers {
  zone green_servers 64k;
  server 10.1.1.5:8001;
}

server {
  listen 1080;
    location /text {
       limit_req zone=green_zone_limit;
       proxy_pass http://green_servers;
    }
}
EOF
```

Reload NGINX on both servers.
```
[plus2/plus3]$ sudo nginx -t

[plus2/plus3]$ sudo nginx -s reload
```

Now we have a new tab on our Dashboards.

![](./images/lab4_step4_1.jpg)

![](./images/lab4_step4_2.jpg)



Now we can test from __docker__ node, using __plus2__ and __plus3__ with ___httpie___. We change previous step's loop to include both Nginx plus servers.
```
[vagrant@docker ~]$ for I in {1..3};do date; curl --fail http://10.1.1.7:1080/text;sleep 10;curl --fail http://10.1.1.8:1080/text;done
Tue jul 17 18:24:13 CEST 2020
APP_VERSION: 1.20
COLOR: green
CONTAINER_NAME: e16df21d4625
CONTAINER_IP:  172.17.0.2
CLIENT_IP: ::ffff:10.1.1.7
CONTAINER_ARCH: linux
HOSTNAME (from environment): e16df21d4625
DATETIME: July 17th 2020, 4:24:13 pm
HEADERS: 
	- host: "green_servers"
	- connection: "close"
	- user-agent: "curl/7.29.0"
	- accept: "*/*"

curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
Tue jul 17 18:24:24 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
Tue jul 17 18:24:34 CEST 2020
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable
curl: (22) The requested URL returned error: 503 Service Temporarily Unavailable

```

>NOTE: We added 10 seconds delay between first requests to each loadbalancer to allow them to sync their configurations.


Notice that only the first request had a response. All other requests received a 503 error, no matter which Nginx server received it. This is due to zone synchronization. We synced __"green_zone_limit"__ zone between servers therefore limit is applied using data from both servers and we defined __rate=1r/m__. Only one request is allowed per minute "in that zone", because both servers are sharind the requests counter.

In this lab we learned how to configure Nginx __zone sharing__ to manage persistence data between nodes in a cluster (set of Nginx hosts sharing zones).


>If several NGINX Plus instances are organized in a cluster, they can share some state data between them, including:
>
>- sticky learn session persistence
>- requests limiting
>- key-value storage
>All NGINX Plus instances can exchange state data with all other members in a cluster, provided that the shared memory zone has the same name on all cluster members.

