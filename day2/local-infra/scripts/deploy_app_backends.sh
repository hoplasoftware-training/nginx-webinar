#!/bin/bash

sudo systemctl restart docker

sudo docker rm -fv green-8001 blue-8002 red-8003 picsum > /dev/null 2>&1





echo -e "\n## Deploying GREEN application on port 8001"

sudo docker run -d --name green-8001 \
-e COLOR=green \
-p 8001:3000 \
-l color=green \
--restart=unless-stopped \
codegazers/colors:1.17


echo -e "\n## Deploying BLUE application on port 8002"

sudo docker run -d --name blue-8002 \
-e COLOR=blue \
-p 8002:3000 \
-l color=blue \
--restart=unless-stopped \
codegazers/colors:1.17


echo -e "\n## Deploying RED application on port 8003"

sudo docker run -d --name red-8003 \
-e COLOR=red \
-p 8003:3000 \
-l color=red \
--restart=unless-stopped \
codegazers/colors:1.17


echo -e "\n## Deploying PICSUM application on port 8005"

sudo docker run -d --name picsum \
-e MAX_NUMBER_OF_PICTURES=20 \
-e MAX_PICTURE_SOURCE_URL=https://picsum.photos/1024 \
-p 8005:80 \
--restart=unless-stopped \
bosix/test-picture-provider
