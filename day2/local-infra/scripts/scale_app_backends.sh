#!/bin/bash -x


for I in 9 10;
do

    sudo docker rm -fv green-${I}001 blue-${I}002 red-${I}003 > /dev/null 2>&1

    echo -e "\n## Deploying new GREEN application on port ${I}001"

    sudo docker run -d --name green-${I}001 \
    -e COLOR=green \
    -p ${I}001:3000 \
    -l color=green \
    --restart=unless-stopped \
    codegazers/colors:1.17


    echo -e "\n## Deploying BLUE application on port ${I}002"

    sudo docker run -d --name blue-${I}002 \
    -e COLOR=blue \
    -p ${I}002:3000 \
    -l color=blue \
    --restart=unless-stopped \
    codegazers/colors:1.17


    echo -e "\n## Deploying RED application on port ${I}003"

    sudo docker run -d --name red-${I}003 \
    -e COLOR=red \
    -p ${I}003:3000 \
    -l color=red \
    --restart=unless-stopped \
    codegazers/colors:1.17
done