#!/bin/bash

sudo systemctl stop firewalld

sudo systemctl disable firewalld

sudo setenforce 0

sudo sed -i "s/SELINUX=.*/SELINUX=disabled/" /etc/sysconfig/selinux

